//
//  NotificationController.h
//  Archery Tools WatchKit Extension
//
//  Created by Andrew Querol on 11/21/14.
//  Copyright (c) 2014 Cerios Game Studios. All rights reserved.
//

#import <WatchKit/WatchKit.h>
#import <Foundation/Foundation.h>

@interface NotificationController : WKUserNotificationInterfaceController

@end
