//
//  SpecialKeyDOM.h
//  Archery Tools
//
//  Created by Andrew Querol on 5/19/14.
//  Copyright (c) 2014 Cerios Game Studios. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SpecialKeyDOM : NSObject
@property NSInteger code;
@property NSInteger width;
@property NSInteger height;
@property NSString *image;
@end
