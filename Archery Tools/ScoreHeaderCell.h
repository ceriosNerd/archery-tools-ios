//
//  ScoreHeaderCell.h
//  Archery Tools
//
//  Created by Andrew Querol on 3/28/12.
//  Copyright (c) 2012 Cerios Game Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScoreHeaderCell : UITableViewHeaderFooterView

@end
