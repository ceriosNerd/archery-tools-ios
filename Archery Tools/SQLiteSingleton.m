//
//  SQLiteSingleton.m
//  Archery Tools
//
//  Created by Andrew Querol on 11/23/14.
//  Copyright (c) 2014 Cerios Game Studios. All rights reserved.
//

#import "SQLiteSingleton.h"
#import "AppDelegate.h"
#import <limits.h>

#define DB_VERSION 2

static sqlite3 *database = nil;
static SQLiteSingleton *sqliteSingleton = nil;
static const NSString *makeDB =
@"PRAGMA foreign_keys=ON; PRAGMA journal_mode=WAL; PRAGMA user_version = 2;"
"CREATE TABLE IF NOT EXISTS score_cards(id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, person TEXT, eventDate INTEGER DEFAULT (strftime('%s','now')), type TEXT, keyboard TEXT, synced BOOLEAN NOT NULL DEFAULT 0 CHECK (synced IN (0,1)));"
"CREATE TABLE IF NOT EXISTS rounds(id INTEGER PRIMARY KEY AUTOINCREMENT, number INTEGER, arrows_per_end INTEGER, average_score REAL DEFAULT 0, distance_text TEXT, ends_per_round INTEGER, hits INTEGER DEFAULT 0, total INTEGER DEFAULT 0, total_shot INTEGER DEFAULT 0, score_card_id INTEGER, synced BOOLEAN NOT NULL DEFAULT 0 CHECK (synced IN (0,1)), FOREIGN KEY(score_card_id) REFERENCES score_cards(id) ON DELETE CASCADE);"
"CREATE TABLE IF NOT EXISTS ends(id INTEGER PRIMARY KEY AUTOINCREMENT, number INTEGER, total INTEGER DEFAULT 0, hits INTEGER DEFAULT 0, running_total INTEGER DEFAULT 0, shots INTEGER DEFAULT 0,round_id INTEGER, synced BOOLEAN NOT NULL DEFAULT 0 CHECK (synced IN (0,1)), FOREIGN KEY(round_id) REFERENCES rounds(id) ON DELETE CASCADE);"
"CREATE TABLE IF NOT EXISTS arrows(id INTEGER PRIMARY KEY AUTOINCREMENT, number INTEGER, value INTEGER DEFAULT -1, sort_order INTEGER NOT NULL DEFAULT -1, image TEXT DEFAULT 'gray', text_value TEXT DEFAULT '', text_color TEXT DEFAULT 'blackColor', end_id INTEGER, synced BOOLEAN NOT NULL DEFAULT 0 CHECK (synced IN (0,1)), FOREIGN KEY(end_id) REFERENCES ends(id) ON DELETE CASCADE);"
"CREATE TABLE IF NOT EXISTS bows(id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, image_path TEXT, synced BOOLEAN NOT NULL DEFAULT 0 CHECK (synced IN (0,1)));"
"CREATE TABLE IF NOT EXISTS sight_configurations(id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, metric BOOLEAN NOT NULL CHECK (metric IN (0,1)) DEFAULT 1, bow_id INTEGER, synced BOOLEAN NOT NULL DEFAULT 0 CHECK (synced IN (0,1)), FOREIGN KEY(bow_id) REFERENCES bows(id) ON DELETE CASCADE);"
"CREATE TABLE IF NOT EXISTS sight_marks(id INTEGER PRIMARY KEY AUTOINCREMENT, distance INTEGER, mark REAL, smaller_units BOOLEAN NOT NULL CHECK (smaller_units IN (0,1)) DEFAULT 0, sight_config_id INTEGER, synced BOOLEAN NOT NULL DEFAULT 0 CHECK (synced IN (0,1)), FOREIGN KEY(sight_config_id) REFERENCES sight_configurations(id) ON DELETE CASCADE);"
"CREATE TABLE IF NOT EXISTS measurements(id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, measurement REAL, bow_id INTEGER, synced BOOLEAN NOT NULL DEFAULT 0 CHECK (synced IN (0,1)), FOREIGN KEY(bow_id) REFERENCES bows(id) ON DELETE CASCADE);"
"CREATE TABLE IF NOT EXISTS notes(id INTEGER PRIMARY KEY AUTOINCREMENT, title TEXT DEFAULT 'New Note', content TEXT, bow_id INTEGER, synced BOOLEAN NOT NULL DEFAULT 0 CHECK (synced IN (0,1)), FOREIGN KEY(bow_id) REFERENCES bows(id) ON DELETE CASCADE);";

@implementation SQLiteSingleton

@synthesize dirtyTables;

+ (SQLiteSingleton *) getInstance {
    if (!sqliteSingleton) {
        sqliteSingleton = [[SQLiteSingleton alloc] init];
    }
    return sqliteSingleton;
}

void sqlite3DataUpdated(void *tag, int type, const char *databaseName, const char *tableName, sqlite3_int64 rowId) {
    SQLiteSingleton *instance = (__bridge SQLiteSingleton *)tag;
    NSString *objCTableName = [NSString stringWithUTF8String:tableName];
    
    if (![[instance dirtyTables] objectForKey:objCTableName]) {
        [[instance dirtyTables] setObject:[[NSMutableIndexSet alloc] init] forKey:objCTableName];
    }
    
    if (rowId > UINT32_MAX) {
        rowId = UINT32_MAX;
    } else if (rowId < 0) {
        rowId = 0;
    }
    [((NSMutableIndexSet *)[[instance dirtyTables] objectForKey:objCTableName]) addIndex:(unsigned int)rowId];
}

- (instancetype)init {
    NSString *sqLiteDb = [[NSURL URLWithString:@"ArcheryTools.sqlite" relativeToURL:[AppDelegate applicationDocumentsDirectory]] absoluteString];
    if (sqlite3_open_v2([sqLiteDb UTF8String], &database, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE | SQLITE_OPEN_FULLMUTEX, NULL) != SQLITE_OK) {
        NSLog(@"Failed to open database!");
        return nil;
    } else {
        char *errorMessage = nil;
        if (sqlite3_exec(database, [makeDB UTF8String], nil, nil, &errorMessage) != SQLITE_OK) {
            NSLog(@"Failed to create tables! Message: %s", errorMessage);
            return nil;
        }
        
        int dbVersion = -1;
        sqlite3_stmt *versionCheck;
        sqlite3_prepare_v2(database, "PRAGMA user_version", -1, &versionCheck, nil);
        if (sqlite3_step(versionCheck) != SQLITE_ROW) {
            dbVersion = 1;
        } else {
            dbVersion = sqlite3_column_int(versionCheck, 0);
        }
        sqlite3_finalize(versionCheck);
        
        if (dbVersion < DB_VERSION) {
            [self attemptTableAlterationFromVersion:dbVersion newVersion:DB_VERSION];
        }
    }
    sqlite3_update_hook(database, &sqlite3DataUpdated, (__bridge void *)(self));
    return [super init];
}

- (void) attemptTableAlterationFromVersion:(int)from newVersion:(int) to {
    char *errorMessage = nil;
    
    // Rename old tables
    sqlite3_exec(database, "ALTER TABLE score_cards RENAME TO old_score_cards", nil, nil, &errorMessage);
    
    // Re-Add the tables
    sqlite3_exec(database, [makeDB UTF8String], nil, nil, &errorMessage);
    
    // Migrate Data
    sqlite3_exec(database, "INSERT INTO score_cards(id, name, person, type, keyboard, synced) SELECT id, name, person, type, keyboard, synced FROM old_score_cards; DROP TABLE old_score_cards", nil, nil, &errorMessage);
    
    // Print any errors
    if (errorMessage) {
         NSLog(@"Failed to alter tables! Message: %s", errorMessage);
    }
}

- (sqlite3 *) getDatabase {
    return database;
}

/*
 * Async Method
 * Try to migrate from any existing CoreData store. This method then writes it out to the sqlite store and purges any old CoreData files from the Documents directory.
 * The successBlock gets called on complete migration, it returns the two possible success outcomes. Successful migration or that there was no CoreData to migrate.
 * The failBlock gets called on any error in the migration, the error codes describe the failure point. CoreData error indicates a error with reading the core data.
 * SQLite error indicates a error writing the CoreData store into the new SQLite store. No files will be removed that is up to the caller in the successBlock.
 * The blocks get executed back on the main queue
 */
- (void)tryToMigrateFromCoreData:(NSPersistentStoreCoordinator *)persistantStore onSuccess:(onSuccess)successBlock onFailure:(onFailure)failBlock {
    dispatch_async(dispatch_queue_create("ArcheryTools: Migration", DISPATCH_QUEUE_SERIAL),^{
        NSManagedObjectContext *backgroundContext = [[NSManagedObjectContext alloc] init];
        backgroundContext.persistentStoreCoordinator = persistantStore;
        
        NSLog(@"Begin Score Card migration begining!");
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *scoreCardEntity = [NSEntityDescription entityForName:@"ScoreCard" inManagedObjectContext:backgroundContext];
        [fetchRequest setEntity:scoreCardEntity];
        
        NSError *error = nil;
        NSArray *scoreCards = [backgroundContext executeFetchRequest:fetchRequest error:&error];
        if (error != nil) {
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            dispatch_async(dispatch_get_main_queue(), ^{
                failBlock(CoreDataError);
            });
        }
        
        char *errorMsg;
        sqlite3_exec([[SQLiteSingleton getInstance] getDatabase], "BEGIN TRANSACTION", NULL, NULL, &errorMsg);
        if (errorMsg != nil) {
            NSLog(@"Unresolved error!\n%s", errorMsg);
            dispatch_async(dispatch_get_main_queue(), ^{
                failBlock(SQLiteError);
            });
        }
        
        NSString *queryString;
        for (NSManagedObject *scoreCard in scoreCards) {
            queryString = [NSString stringWithFormat:@"INSERT INTO score_cards(name, type, keyboard) VALUES ('%@', '%@', '%@');", [SQLiteSingleton parseString:[scoreCard valueForKey:@"eventName"]], [SQLiteSingleton parseString:[[scoreCard valueForKey:@"event"] valueForKey:@"typeName"]], [SQLiteSingleton parseString:[[scoreCard valueForKey:@"event"] valueForKey:@"keyboardFile"]]];
            sqlite3_exec([[SQLiteSingleton getInstance] getDatabase], [queryString UTF8String], nil, nil, &errorMsg);
            sqlite3_int64 scoreCardId = sqlite3_last_insert_rowid([[SQLiteSingleton getInstance] getDatabase]);
            
            for (NSManagedObject *round in [[scoreCard valueForKey:@"event"] mutableOrderedSetValueForKey:@"rounds"]) {
                queryString = [NSString stringWithFormat:@"INSERT INTO rounds(number, arrows_per_end, average_score, distance_text, ends_per_round, hits, total, total_shot, score_card_id) VALUES ('%d', '%d', '%f', '%@', '%d', '%d', '%d', '%d', '%lld');", [(NSNumber *)[round valueForKey:@"roundNumber"] intValue], [(NSNumber *)[round valueForKey:@"arrowsPerEnd"] intValue], [(NSNumber *)[round valueForKey:@"avgScore"] floatValue], [SQLiteSingleton parseString:[round valueForKey:@"distanceText"]], [(NSNumber *)[round valueForKey:@"endsPerRound"] intValue], [(NSNumber *)[round valueForKey:@"hits"] intValue], [(NSNumber *)[round valueForKey:@"total"] intValue], [(NSNumber *)[round valueForKey:@"totalShot"] intValue], scoreCardId];
                sqlite3_exec([[SQLiteSingleton getInstance] getDatabase], [queryString UTF8String], nil, nil, &errorMsg);
                sqlite3_int64 roundId = sqlite3_last_insert_rowid([[SQLiteSingleton getInstance] getDatabase]);
                
                for (NSManagedObject *end in [round mutableOrderedSetValueForKey:@"ends"]) {
                    queryString = [NSString stringWithFormat:@"INSERT INTO ends(number, total, hits, running_total, shots, round_id) VALUES ('%d', '%d', '%d', '%d', '%d', '%lld');", [(NSNumber *)[end valueForKey:@"endNumber"] intValue], [(NSNumber *)[end valueForKey:@"endTotal"] intValue], [(NSNumber *)[end valueForKey:@"hits"] intValue], [(NSNumber *)[end valueForKey:@"runningTotal"] intValue], [(NSNumber *)[end valueForKey:@"shots"] intValue], roundId];
                    sqlite3_exec([[SQLiteSingleton getInstance] getDatabase], [queryString UTF8String], nil, nil, &errorMsg);
                    sqlite3_int64 endId = sqlite3_last_insert_rowid([[SQLiteSingleton getInstance] getDatabase]);
                    
                    int order = 1;
                    for (NSManagedObject *arrow in [end mutableOrderedSetValueForKey:@"arrows"]) {
                        queryString = [NSString stringWithFormat:@"INSERT INTO arrows(number, value, sort_order, image, text_value, text_color, end_id) VALUES ('%d', '%d', '%d', '%@', '%@', '%@', '%lld');", [(NSNumber *)[arrow valueForKey:@"arrowNumber"] intValue], [(NSNumber *)[arrow valueForKey:@"arrowValue"] intValue], [arrow valueForKey:@"arrowText"] == nil ? -1 : order, [SQLiteSingleton parseString:[arrow valueForKey:@"arrowImage"]], [SQLiteSingleton parseString:[arrow valueForKey:@"arrowText"]], [SQLiteSingleton parseString:[arrow valueForKey:@"textColor"]], endId];
                        sqlite3_exec([[SQLiteSingleton getInstance] getDatabase], [queryString UTF8String], nil, nil, &errorMsg);
                        order++;
                    }
                }
            }
        }
        
        sqlite3_exec([[SQLiteSingleton getInstance] getDatabase], "END TRANSACTION", NULL, NULL, &errorMsg);
        if (errorMsg != nil) {
            NSLog(@"Unresolved error!\n%s", errorMsg);
            dispatch_async(dispatch_get_main_queue(), ^{
                failBlock(SQLiteError);
            });
        }
        NSLog(@"Score Card migration completed sucessfully!");
        
        NSLog(@"Bow Card migration begining!");
        NSEntityDescription *bowEntity = [NSEntityDescription entityForName:@"Bow" inManagedObjectContext:backgroundContext];
        [fetchRequest setEntity:bowEntity];
        
        NSArray *bows = [backgroundContext executeFetchRequest:fetchRequest error:&error];
        if (error != nil) {
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            dispatch_async(dispatch_get_main_queue(), ^{
                failBlock(CoreDataError);
            });
        }
        
        sqlite3_exec([[SQLiteSingleton getInstance] getDatabase], "BEGIN TRANSACTION", NULL, NULL, &errorMsg);
        if (errorMsg != nil) {
            NSLog(@"Unresolved error!\n%s", errorMsg);
            dispatch_async(dispatch_get_main_queue(), ^{
                failBlock(SQLiteError);
            });
        }
        
        for (NSManagedObject *bow in bows) {
            NSData *imageData = [bow valueForKey:@"image"];
            NSURL *imageUrl = nil;
            if (imageData != nil && [imageData length] > 0) {
                NSURL *imageUrl = [NSURL URLWithString:[NSString stringWithFormat:@"images/%@-Picture@%lu.png", [SQLiteSingleton parseString:[bow valueForKey:@"name"]], (unsigned long)time(NULL)] relativeToURL:[AppDelegate applicationDocumentsDirectory]];
                [imageData writeToURL:imageUrl atomically:true];
            }
            queryString = [NSString stringWithFormat:@"INSERT INTO bows(name, image_path) VALUES ('%@', '%@');", [SQLiteSingleton parseString:[bow valueForKey:@"name"]], imageUrl ? [imageUrl absoluteString] : @""];
            sqlite3_exec([[SQLiteSingleton getInstance] getDatabase], [queryString UTF8String], nil, nil, &errorMsg);
            sqlite3_int64 bowId = sqlite3_last_insert_rowid([[SQLiteSingleton getInstance] getDatabase]);
            
            for (NSManagedObject *sightConfig in [bow mutableSetValueForKey:@"sightConfigs"]) {
                queryString = [NSString stringWithFormat:@"INSERT INTO sight_configurations(name, metric, bow_id) VALUES ('%@', '%d', '%lld');", [SQLiteSingleton parseString:[sightConfig valueForKey:@"name"]], [(NSNumber *)[sightConfig valueForKey:@"metric"] intValue], bowId];
                sqlite3_exec([[SQLiteSingleton getInstance] getDatabase], [queryString UTF8String], nil, nil, &errorMsg);
                sqlite3_int64 sightConfigId = sqlite3_last_insert_rowid([[SQLiteSingleton getInstance] getDatabase]);
                
                for (NSManagedObject *sightMark in [sightConfig mutableSetValueForKey:@"sightMarks"]) {
                    queryString = [NSString stringWithFormat:@"INSERT INTO sight_marks(distance, mark, smaller_units, sight_config_id) VALUES ('%d', '%f', '%d', '%lld');", [(NSNumber *)[sightMark valueForKey:@"distance"] intValue], [(NSDecimalNumber *)[sightMark valueForKey:@"mark"] floatValue], [(NSNumber *)[sightMark valueForKey:@"feet"] intValue], sightConfigId];
                    sqlite3_exec([[SQLiteSingleton getInstance] getDatabase], [queryString UTF8String], nil, nil, &errorMsg);
                }
            }
            
            for (NSManagedObject *measurement in [bow mutableSetValueForKey:@"measurements"]) {
                queryString = [NSString stringWithFormat:@"INSERT INTO measurements(name, measurement, bow_id) VALUES ('%@', '%f', '%lld');", [SQLiteSingleton parseString:[measurement valueForKey:@"name"]], [(NSDecimalNumber *)[measurement valueForKey:@"setting"] floatValue], bowId];
                sqlite3_exec([[SQLiteSingleton getInstance] getDatabase], [queryString UTF8String], nil, nil, &errorMsg);
            }
            
            for (NSManagedObject *note in [bow mutableSetValueForKey:@"notes"]) {
                queryString = [NSString stringWithFormat:@"INSERT INTO notes(title, content, bow_id) VALUES ('%@', '%@', '%lld');", [SQLiteSingleton parseString:[note valueForKey:@"title"]], [SQLiteSingleton parseString:[note valueForKey:@"text"]], bowId];
                sqlite3_exec([[SQLiteSingleton getInstance] getDatabase], [queryString UTF8String], nil, nil, &errorMsg);
            }
        }
        
        sqlite3_exec([[SQLiteSingleton getInstance] getDatabase], "END TRANSACTION", NULL, NULL, &errorMsg);
        if (errorMsg != nil) {
            NSLog(@"Unresolved error!\n%s", errorMsg);
            dispatch_async(dispatch_get_main_queue(), ^{
                failBlock(SQLiteError);
            });
        }
        NSLog(@"Bow migration completed sucessfully!");
        
        // Lets signal the completion
        dispatch_async(dispatch_get_main_queue(), ^{
            successBlock(MigrationDone);
        });
    });
}

+ (NSString *) parseString:(NSString *) input {
    if (input == nil)
        return @"";
    return input;
}

@end
