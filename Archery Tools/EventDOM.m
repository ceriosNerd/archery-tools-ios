//
//  RoundDOM.m
//  Archery Tools
//
//  Created by Andrew Querol on 5/12/14.
//  Copyright (c) 2014 Cerios Game Studios. All rights reserved.
//

#import "EventDOM.h"
#import "RoundDOM.h"

@implementation EventDOM

@synthesize keyboardFile;
@synthesize name;
@synthesize rounds;

- (id) init {
    self = [super init];
    if (self) {
        rounds = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)addRound:(RoundDOM *)round {
    [rounds addObject:round];
}

@end
