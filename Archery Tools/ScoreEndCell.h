//
//  ScoreEndCell.h
//  Archery Tools
//
//  Created by Andrew Querol on 3/28/12.
//  Copyright (c) 2012 Cerios Game Studios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KeyboardDOM.h"

@class ArcheryTextLabel;
@class End;
@class Arrow;

@protocol ScoreEndCellDelgate <NSObject>

- (void) updateLabels: (NSIndexPath *) index;
- (void) infoPush: (int) sender;

@end

@interface ScoreEndCell : UITableViewCell <KeyboardDelegate>

@property (strong, nonatomic) IBOutletCollection(ArcheryTextLabel) NSArray *archeryTextLabels;
@property (weak, nonatomic) IBOutlet UILabel *endNumberTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *hitsNumberTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *endTotalTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *runningTotalTextLabel;

@property NSInteger selectedTag;
@property id<ScoreEndCellDelgate> delgate;
@property NSMutableOrderedSet *archeryTextFields;
@property UITableView *tableView;
@property NSInteger numberOfArrows;
@property UIView *keyboard;

- (void) clearSelection;
- (void) nextArcheryTextLabel;
- (void) prevArcheryTextLabel;
- (void) setRunningTotal: (NSString *) total;
- (void) setEndTotal: (NSString *) endTotal;
- (void) setHits: (NSString *) hits;
- (void) initCell;
- (void) refreshText;
- (void) setEndId:(int) end;
- (int) getEndId;

+ (void) clearLabel: (ArcheryTextLabel *) label;

@end
