//
//  LegalViewController.m
//  Archery Tools
//
//  Created by Andrew Querol on 12/24/14.
//  Copyright (c) 2014 Cerios Game Studios. All rights reserved.
//

#import "LegalViewController.h"

@implementation LegalViewController

- (void)viewWillLayoutSubviews {
    [self.legalTextView setContentOffset:CGPointZero animated:NO];
    [super viewWillLayoutSubviews];
}

@end
