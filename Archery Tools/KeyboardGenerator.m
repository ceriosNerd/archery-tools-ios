//
//  KeyboardGenerator.m
//  Archery Tools
//
//  Created by Andrew Querol on 5/14/14.
//  Copyright (c) 2014 Cerios Game Studios. All rights reserved.
//

#import "KeyboardGenerator.h"
#import "KeyboardDOM.h"
#import "RowDOM.h"
#import "KeyDOM.h"
#import "ButtonCell.h"
#import "SpecialKeyDOM.h"
#import "ScoreEndCell.h"
#import "ArcheryTextLabel.h"

static __weak id currentFirstResponder;

@implementation UIResponder (FirstResponder)

+(id)currentFirstResponder {
    currentFirstResponder = nil;
    [[UIApplication sharedApplication] sendAction:@selector(findFirstResponder:) to:nil from:nil forEvent:nil];
    return currentFirstResponder;
}

-(void)findFirstResponder:(id)sender {
    currentFirstResponder = self;
}

@end

@implementation KeyboardGenerator

@synthesize tempKeyboard;
@synthesize currentRow;

- (KeyboardDOM *) generateKeyboardFromFile:(NSString *)keyboardFile {
    tempKeyboard = [[KeyboardDOM alloc] init];
    [tempKeyboard setFileName:keyboardFile];
    [self parseXML:[NSBundle URLForResource:[keyboardFile stringByDeletingPathExtension] withExtension:@"xml" subdirectory:nil inBundleWithURL:[[NSBundle mainBundle] bundleURL]]];
    return tempKeyboard;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    KeyboardDOM *key = [collectionView.layer valueForKey:@"keyboard"];
    return [key.rows count] + 1; // Don't Like this constant for the special row
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    KeyboardDOM *keyboard = [collectionView.layer valueForKey:@"keyboard"];
    if ([keyboard.rows count] <= section) {
        return [keyboard.specialKeys count];
    }
    return [[[keyboard.rows objectAtIndex:section] keys] count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    KeyboardDOM *keyboardDOM = [collectionView.layer valueForKey:@"keyboard"];
    ButtonCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"key" forIndexPath:indexPath];
    UIButton *button = [cell button];
    
    if ([keyboardDOM.rows count] <= indexPath.section) {
        SpecialKeyDOM *specialKey = [keyboardDOM.specialKeys objectAtIndex:indexPath.row];
        [button addTarget:self action:@selector(specialKeyPressed:) forControlEvents:UIControlEventTouchUpInside];
        if (specialKey.code == 3) {
            // Ugly I don't like this static text but it'll do for now
            [button setTitle:@"Close" forState:UIControlStateNormal];
            [button.titleLabel setFont:[button.titleLabel.font fontWithSize:25]];
        } else if (specialKey.code == 1) {
            // Unicode 0x232B
            [button setTitle:@"⌫" forState:UIControlStateNormal];
        }
        if (specialKey.code != 3) {
            [button setBackgroundImage:[UIImage imageNamed:specialKey.image] forState:UIControlStateNormal];
        }
        [button.layer setValue:specialKey forKey:@"key"];
        return cell;
    }
    
    KeyDOM *key = [[[keyboardDOM.rows objectAtIndex:indexPath.section] keys] objectAtIndex:indexPath.row];
    [button.layer setValue:key forKey:@"keyDOM"];
    if (![UIImage imageNamed:[key imageFile]]) {
        [button setBackgroundImage:[UIImage imageNamed:@"white"] forState:UIControlStateNormal];
    } else {
        [button setBackgroundImage:[UIImage imageNamed:[key imageFile]] forState:UIControlStateNormal];
    }
    [button setTitle:[key keyText] forState:UIControlStateNormal];
    if ([UIColor respondsToSelector:NSSelectorFromString(key.textColor)]) {
        [button setTitleColor:[UIColor performSelector:NSSelectorFromString(key.textColor)] forState:UIControlStateNormal];
    } else {
        NSLog(@"Invalid color in keyboard XML!\nXML File:%@\nText Color:%@", tempKeyboard.fileName, key.textColor);
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    }
    [button addTarget:self action:@selector(keyPressed:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    KeyboardDOM *keyboardDOM = [collectionView.layer valueForKey:@"keyboard"];
    if ([keyboardDOM.rows count] <= indexPath.section) {
        SpecialKeyDOM *specialKey = [keyboardDOM.specialKeys objectAtIndex:indexPath.row];
        return CGSizeMake([specialKey width], [specialKey height]);
    } else {
        return CGSizeMake(40, 40);
    }
}

- (UIView *)generateKeyboardViewFromDOM:(KeyboardDOM *)keyboardDOM {
    UIVisualEffectView *blurEffect = [[UIVisualEffectView alloc] initWithEffect: [UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight]];
    UIView *container = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 160)];
    [container insertSubview:blurEffect atIndex:0];
    [container setBackgroundColor:[UIColor clearColor]];
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [flowLayout setSectionInset:UIEdgeInsetsMake(5,5,5,5)];
    UICollectionView *collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, 320, 160) collectionViewLayout:flowLayout];
    [collectionView registerNib:[UINib nibWithNibName:@"key" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"key"];
    [collectionView.layer setValue:keyboardDOM forKey:@"keyboard"];
    [collectionView setDataSource:self];
    [collectionView setDelegate:self];
    [collectionView setBackgroundColor:[UIColor clearColor]];
    [collectionView setBackgroundView:nil];
    [collectionView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [container addSubview:collectionView];
    [container bringSubviewToFront:collectionView];
    [container addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[collection(>=300,<=320)]" options:0 metrics:nil views:@{@"collection": collectionView}]];
    [container addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[collection(>=160)]|" options:0 metrics:nil views:@{@"collection": collectionView}]];
    [container addConstraint:[NSLayoutConstraint constraintWithItem:collectionView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:container attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0]];
    return container;
}

- (void) keyPressed:(id) sender {
    KeyDOM *keyDOM = [((UIView *)sender).layer valueForKey:@"keyDOM"];
    // Messy....
    ScoreEndCell *caller = ((ArcheryTextLabel *)[UIResponder currentFirstResponder]).parent;
    [caller keyPress:keyDOM.keyText integerValue:keyDOM.keyValue.integerValue sortOrder:keyDOM.keyRank.integerValue backgroundImage:[keyDOM imageFile]  textColor:[keyDOM textColor]];
}

- (void) specialKeyPressed:(id) sender {
    SpecialKeyDOM *specialKey = [((UIView *)sender).layer valueForKey:@"key"];
    // Messy....
    ScoreEndCell *caller = ((ArcheryTextLabel *)[UIResponder currentFirstResponder]).parent;
    switch (specialKey.code) {
        case 1:
            [caller deletePress];
            break;
        case 2:
            [caller infoPress];
            break;
        case 3:
            [caller closeKeyboard];
        default:
            break;
    }
}

- (void)parseXML:(NSURL *)url {
    NSXMLParser *parser = [[NSXMLParser alloc] initWithContentsOfURL:url];
    [parser setDelegate:self];
    [parser setShouldProcessNamespaces:NO];
    [parser setShouldReportNamespacePrefixes:NO];
    [parser setShouldResolveExternalEntities:NO];
    if (![parser parse]) {
        NSLog(@"Keyboard generation error!\n%@", [[parser parserError] debugDescription]);
    }
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    if ([elementName isEqualToString:@"keyboard"]) {
        [tempKeyboard setName:attributeDict[@"name"]];
        tempKeyboard.rows = [[NSMutableArray alloc] init];
        tempKeyboard.specialKeys = [[NSMutableArray alloc] init];
    } else if ([elementName isEqualToString:@"row"]) {
        currentRow = [[RowDOM alloc] init];
        currentRow.keys = [[NSMutableOrderedSet alloc] init];
    } else if ([elementName isEqualToString:@"key"]) {
        KeyDOM *key = [[KeyDOM alloc] init];
        [key setKeyText:attributeDict[@"text"]];
        [key setKeyValue:[NSNumber numberWithInt:[attributeDict[@"value"] intValue]]];
        [key setKeyRank:[NSNumber numberWithInt:[attributeDict[@"rank"] intValue]]];
        [key setImageFile:attributeDict[@"color"]];
        [key setTextColor:[attributeDict[@"textColor"] stringByAppendingString:@"Color"]];
        [currentRow.keys addObject:key];
    } else if ([elementName isEqualToString:@"specialKey"]) {
        SpecialKeyDOM *specialKey = [[SpecialKeyDOM alloc] init];
        [specialKey setCode:[attributeDict[@"code"] integerValue]];
        [specialKey setHeight:[attributeDict[@"height"] integerValue]];
        [specialKey setWidth:[attributeDict[@"width"] integerValue]];
        [specialKey setImage:attributeDict[@"image"]];
        [tempKeyboard.specialKeys addObject:specialKey];
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    if ([elementName isEqualToString:@"row"]) {
        if ([currentRow.keys count] <= 0)
            return;
        [tempKeyboard.rows addObject:currentRow];
    }
}

@end
