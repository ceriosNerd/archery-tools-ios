//
//  EventTypeViewController.m
//  Archery Tools
//
//  Created by Andrew Querol on 3/26/12.
//  Copyright (c) 2012 Cerios Game Studios. All rights reserved.
//

#import "EventTypeViewController.h"
#import "ScoreViewController.h"
#import "EventDOM.h"
#import "RoundDOM.h"
#import "CategoryDOM.h"
#import "SQLiteSingleton.h"

@implementation EventTypeViewController

@synthesize eventTypePicker;
@synthesize categories;
@synthesize currentEvents;
@synthesize currentCategoryName;
@synthesize xmlLoaded;
@synthesize xmlParser;
@synthesize currentEvent;
@synthesize currentElementText;
@synthesize currentRound;
@synthesize currentSubtypeString;
@synthesize currentKeyboard;

- (void) viewDidLoad {
    [eventTypePicker setDelegate:self];
    [eventTypePicker setDataSource:self];
    
    xmlLoaded = false;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self parseXML];
    });
    [super viewDidLoad];
}

- (void) viewWillLayoutSubviews {
    [eventTypePicker setUserInteractionEnabled:true];
}

- (void) parseXML {
    NSURL *xmlUrl = [NSBundle URLForResource:@"preset_rounds" withExtension:@"xml" subdirectory:nil inBundleWithURL:[[NSBundle mainBundle] bundleURL]];
    categories = [[NSMutableArray alloc] init];
    xmlParser = [[NSXMLParser alloc] initWithContentsOfURL:xmlUrl];
    [xmlParser setDelegate:self];
    [xmlParser setShouldProcessNamespaces:NO];
    [xmlParser setShouldReportNamespacePrefixes:NO];
    [xmlParser setShouldResolveExternalEntities:NO];
    if (![xmlParser parse]) {
        NSLog(@"Error parsing XML");
    }
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    if ([elementName isEqualToString:@"category"]) {
        currentEvents = [[NSMutableArray alloc] init];
        currentCategoryName = attributeDict[@"name"];
    } else if ([elementName isEqualToString:@"event"]) {
        currentEvent = [[EventDOM alloc] init];
        [currentEvent setName:attributeDict[@"name"]];
    } else if ([elementName isEqualToString:@"round"]) {
        currentRound = [[RoundDOM alloc] init];
    } else if ([elementName isEqualToString:@"subtype"]) {
        currentSubtypeString = attributeDict[@"type"];
    }
    // The content will always be after a start tag
    currentElementText = nil;
}

/*
 * This gets the text inbetween the tag
 */
-(void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    if (currentElementText == nil) {
        currentElementText = [[NSMutableString alloc] initWithString:string];
    } else {
        [currentElementText appendString:string];
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    if ([elementName isEqualToString:@"category"]) {
        CategoryDOM *category = [[CategoryDOM alloc] init];
        [category setCategoryName:currentCategoryName];
        [category setEventDOMArray:currentEvents];
        [categories addObject:category];
        currentEvents = nil;
        currentCategoryName = nil;
    } else if ([elementName isEqualToString:@"keyboard"]) {
        if (currentElementText != nil) {
            currentKeyboard = currentElementText;
        }
    } else if ([elementName isEqualToString:@"distance"]) {
        if (currentRound != nil && currentElementText != nil) {
            [currentRound setDistance:currentElementText];
        }
    } else if ([elementName isEqualToString:@"arrowsPerEnd"]) {
        if (currentRound != nil && currentElementText != nil) {
            [currentRound setArrowsPerEnd:[NSNumber numberWithInt:(int)[currentElementText integerValue]]];
        }
    } else if ([elementName isEqualToString:@"endsPerRound"]) {
        if (currentRound != nil && currentElementText != nil) {
            [currentRound setEndsPerRound:[NSNumber numberWithInt:(int)[currentElementText integerValue]]];
        }
    } else if ([elementName isEqualToString:@"round"]) {
        if (currentRound != nil && currentEvent != nil) {
            [currentEvent addRound:currentRound];
            currentRound = nil;
        }
    } else if ([elementName isEqualToString:@"subtype"]) {
        NSString *curentEventName = [currentEvent name];
        [currentEvent setKeyboardFile:currentKeyboard];
        [currentEvent setName:[curentEventName stringByAppendingString:[@" " stringByAppendingString:currentSubtypeString]]];
        [currentEvents addObject:currentEvent];
        currentEvent = [[EventDOM alloc] init];
        [currentEvent setName:curentEventName];
        currentSubtypeString = nil;
    } else if ([elementName isEqualToString:@"event"]) {
        if([[currentEvent rounds] count] > 0) {
            [currentEvent setKeyboardFile:currentKeyboard];
            [currentEvents addObject:currentEvent];
            currentEvent = nil;
        }
    }
    currentElementText = nil;
}

- (void)parserDidEndDocument:(NSXMLParser *)parser {
    dispatch_sync(dispatch_get_main_queue(), ^{
        xmlLoaded = true;
        [eventTypePicker reloadAllComponents];
    });
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 2;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if (!xmlLoaded)
        return 0;
    if (component == 0) {
        return [categories count];
    } else {
        if ([categories count] > [eventTypePicker selectedRowInComponent:0]) {
            return [((CategoryDOM *)[categories objectAtIndex:[eventTypePicker selectedRowInComponent:0]]).eventDOMArray count];
        }
        return 0;
    }
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    float width = pickerView.bounds.size.width;
    float component1Size = 100;
    if (component == 0) {
        return component1Size;
    } else {
        return width - component1Size;
    }
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if (!xmlLoaded)
        return @"";
    if (component == 0) {
        if ([categories count] > row) {
            return [[categories objectAtIndex:row] categoryName];
        }
        return nil;
    } else {
        if ([categories count] > [eventTypePicker selectedRowInComponent:0]) {
            CategoryDOM *category = (CategoryDOM *)[categories objectAtIndex:[eventTypePicker selectedRowInComponent:0]];
            
            if ([[category eventDOMArray] count] > row) {
                return [[[category eventDOMArray] objectAtIndex:row] name];
            }
            return nil;
        }
        return nil;
    }
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if (component == 0) {
        [pickerView reloadComponent:1];
    }
}

- (void)viewDidUnload {
    [self setEventTypePicker:nil];
    [super viewDidUnload];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.destinationViewController class] == [ScoreViewController class]) {
        ScoreViewController *controller = segue.destinationViewController;
        [controller setScoreCardId:[((NSNumber *) sender) intValue]];
    }
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    return true;
}

- (IBAction)onCreateClick:(id)sender {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"New Event" message:nil delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Start Event", nil];
    [alertView setAlertViewStyle:UIAlertViewStyleLoginAndPasswordInput];
    [[alertView textFieldAtIndex:0] setPlaceholder:@"Archer's Name"];
    [[alertView textFieldAtIndex:0] setAutocapitalizationType:UITextAutocapitalizationTypeWords];
    [[alertView textFieldAtIndex:0] setKeyboardType:UIKeyboardTypeDefault];
    [[alertView textFieldAtIndex:1] setPlaceholder:@"Event Name"];
    [[alertView textFieldAtIndex:1] setAutocapitalizationType:UITextAutocapitalizationTypeWords];
    [[alertView textFieldAtIndex:1] setKeyboardType:UIKeyboardTypeDefault];
    [[alertView textFieldAtIndex:1] setSecureTextEntry:false];
    [alertView show];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
        [self performSegueWithIdentifier:@"scoringPush" sender:[NSNumber numberWithLongLong:[self generateScoreCard:[alertView textFieldAtIndex:0].text eventName:[alertView textFieldAtIndex:1].text date:nil]]];
    }
}

- (BOOL)alertViewShouldEnableFirstOtherButton:(UIAlertView *)alertView {
    if ([[alertView textFieldAtIndex:0].text length] > 0 && [[alertView textFieldAtIndex:1].text length] > 0)
        return true;
    return false;
}

- (IBAction)onPracticeClick:(id)sender {
    [self performSegueWithIdentifier:@"scoringPush" sender:[NSNumber numberWithLongLong:[self generateScoreCard:nil eventName:nil date:nil]]];
}

- (long long) generateScoreCard:(NSString *)archersName eventName:(NSString *)eventName date:(NSDate *)date {
    if ([eventTypePicker selectedRowInComponent:0] < 0 || [eventTypePicker selectedRowInComponent:1] < 0)
        return -1;
    CategoryDOM *categoryDOM = (CategoryDOM *) [categories objectAtIndex:[eventTypePicker selectedRowInComponent:0]];
    EventDOM *eventDOM = [categoryDOM.eventDOMArray objectAtIndex:[eventTypePicker selectedRowInComponent:1]];
    
    NSString *scoreCardQuery = [NSString stringWithFormat:@"INSERT INTO score_cards(name, person, type, keyboard) VALUES ('%@', '%@', '%@', '%@');", eventName == nil ? @"Practice Session" : eventName, archersName == nil ? @"Owner" : archersName, eventDOM.name, eventDOM.keyboardFile];
    sqlite3_exec([[SQLiteSingleton getInstance] getDatabase], [scoreCardQuery UTF8String], nil, nil, nil);
    sqlite3_int64 scoreCardId = sqlite3_last_insert_rowid([[SQLiteSingleton getInstance] getDatabase]);
    
    int roundNumber = 1;
    for (RoundDOM *roundDOM in eventDOM.rounds) {
        NSString *roundQuery = [NSString stringWithFormat:@"INSERT INTO rounds(number, arrows_per_end, distance_text, ends_per_round, score_card_id) VALUES ('%d', '%d', '%@', '%d', '%lld');", roundNumber, [roundDOM.arrowsPerEnd intValue], roundDOM.distance, [roundDOM.endsPerRound intValue], scoreCardId];
        sqlite3_exec([[SQLiteSingleton getInstance] getDatabase], [roundQuery UTF8String], nil, nil, nil);
        sqlite3_int64 roundId = sqlite3_last_insert_rowid([[SQLiteSingleton getInstance] getDatabase]);
        
        for (int e = 0; e < roundDOM.endsPerRound.intValue; e++) {
            NSString *endQuery = [NSString stringWithFormat:@"INSERT INTO ends(number, round_id) VALUES ('%d', '%lld');", (e + 1), roundId];
            sqlite3_exec([[SQLiteSingleton getInstance] getDatabase], [endQuery UTF8String], nil, nil, nil);
            sqlite3_int64 endId = sqlite3_last_insert_rowid([[SQLiteSingleton getInstance] getDatabase]);
            
            for (int a = 0; a < roundDOM.arrowsPerEnd.intValue; a++) {
                NSString *arrowQuery = [NSString stringWithFormat:@"INSERT INTO arrows(number, end_id) VALUES ('%d', '%lld');", a, endId];
                sqlite3_exec([[SQLiteSingleton getInstance] getDatabase], [arrowQuery UTF8String], nil, nil, nil);
            }
        }
        roundNumber++;
    }
    
    return scoreCardId;
}

@end
