//
//  GenerateViewController.m
//  Archery Tools
//
//  Created by Andrew Querol on 3/24/12.
//  Copyright (c) 2012 Cerios Game Studios. All rights reserved.
//

#import "GenerateViewController.h"
#import "Regression.h"
#import "SQLiteSingleton.h"

@implementation GenerateViewController

@synthesize distance1TextField;
@synthesize distance2TextField;
@synthesize distance3TextField;
@synthesize sightMark1TextField;
@synthesize sightMark2TextField;
@synthesize sightMark3TextField;
@synthesize sightConfigId;
@synthesize meterYardLabel;
@synthesize incrementTextField;
@synthesize stepper;
@synthesize bunnyMarksLabel;
@synthesize bunnyMarksSwitch;

- (void) viewDidLoad {
    sqlite3_stmt *sightStatement;
    NSString *sightQuery = [NSString stringWithFormat:@"SELECT metric FROM sight_configurations WHERE `id`='%lli' LIMIT 1", [sightConfigId longLongValue]];
    sqlite3_prepare_v2([[SQLiteSingleton getInstance] getDatabase], [sightQuery UTF8String], -1, &sightStatement, nil);
    
    if (sqlite3_step(sightStatement) != SQLITE_ROW) {
        NSLog(@"Failed to retreive the sight configuration for %lli", [sightConfigId longLongValue]);
        return;
    }
    
    [meterYardLabel setText:((sqlite3_column_int(sightStatement, 0) == true) ? @"M" : @"Yd")];
    [stepper setValue:5];
    [stepper setMaximumValue:20];
    [stepper setMinimumValue:1];
    [stepper setStepValue:1];
    if (sqlite3_column_int(sightStatement, 0) == true) {
        [bunnyMarksLabel setHidden:true];
        [bunnyMarksLabel setEnabled:false];
        [bunnyMarksSwitch setHidden:true];
        [bunnyMarksSwitch setEnabled:false];
    }
    sqlite3_finalize(sightStatement);
    
    [super viewDidLoad];
}

- (void)viewDidUnload {
    [self setDistance1TextField:nil];
    [self setDistance2TextField:nil];
    [self setDistance3TextField:nil];
    [self setSightMark1TextField:nil];
    [self setSightMark2TextField:nil];
    [self setSightMark3TextField:nil];
    [super viewDidUnload];
}

- (IBAction)onDoneClick:(id)sender {
    if ([self checkUserData]) {
        NSMutableDictionary *valueDict = [NSMutableDictionary dictionary];
        [valueDict setObject:[NSDecimalNumber decimalNumberWithString:sightMark1TextField.text] forKey:[NSDecimalNumber decimalNumberWithString:distance1TextField.text]];
        [valueDict setObject:[NSDecimalNumber decimalNumberWithString:sightMark2TextField.text] forKey:[NSDecimalNumber decimalNumberWithString:distance2TextField.text]];
        if (sightMark3TextField.text.length > 0 ||distance3TextField.text.length > 0) {
            [valueDict setObject:[NSDecimalNumber decimalNumberWithString:sightMark3TextField.text] forKey:[NSDecimalNumber decimalNumberWithString:distance3TextField.text]];
        }
        
        for (int i = 0; i <= 100; i+=stepper.value) {
            if (i == 0)
                continue;
            
            NSNumber *distance = nil;
            NSDecimalNumber *mark = nil;
            
            if (i == distance1TextField.text.integerValue) {
                distance = [NSNumber numberWithInteger:distance1TextField.text.integerValue];
                mark = [NSDecimalNumber decimalNumberWithString:sightMark1TextField.text];
            } else if (i == distance2TextField.text.integerValue) {
                distance = [NSNumber numberWithInteger:distance2TextField.text.integerValue];
                mark = [NSDecimalNumber decimalNumberWithString:sightMark2TextField.text];
            } else if (i == distance3TextField.text.integerValue) {
                distance = [NSNumber numberWithInteger:distance3TextField.text.integerValue];
                mark = [NSDecimalNumber decimalNumberWithString:sightMark3TextField.text];
            } else {
                distance = [NSNumber numberWithInt:i];
                mark = [Regression powerRegressWithData:valueDict requestedValue:[NSNumber numberWithInt:i]];
            }
            
            sqlite3_exec([[SQLiteSingleton getInstance] getDatabase], [[NSString stringWithFormat:@"INSERT INTO sight_marks(distance, mark, sight_config_id) VALUES ('%i', '%f', '%lli');", [distance intValue], [mark floatValue], [sightConfigId longLongValue]] UTF8String], nil, nil, nil);
        }
        
        if (bunnyMarksSwitch.isOn) {
            sqlite3_exec([[SQLiteSingleton getInstance] getDatabase], [[NSString stringWithFormat:@"INSERT INTO sight_marks(distance, mark, smaller_units, sight_config_id) VALUES ('%i', '%f', '%i', '%lli');", 11, [[Regression powerRegressWithData:valueDict requestedValue:[NSNumber numberWithInt:11]] floatValue], true, [sightConfigId longLongValue]] UTF8String], nil, nil, nil);
            sqlite3_exec([[SQLiteSingleton getInstance] getDatabase], [[NSString stringWithFormat:@"INSERT INTO sight_marks(distance, mark, smaller_units, sight_config_id) VALUES ('%i', '%f', '%i', '%lli');", 35, [[Regression powerRegressWithData:valueDict requestedValue:[NSNumber numberWithDouble:((double)35/(double)3)]] floatValue], true, [sightConfigId longLongValue]] UTF8String], nil, nil, nil);
            sqlite3_exec([[SQLiteSingleton getInstance] getDatabase], [[NSString stringWithFormat:@"INSERT INTO sight_marks(distance, mark, smaller_units, sight_config_id) VALUES ('%i', '%f', '%i', '%lli');", 30, [[Regression powerRegressWithData:valueDict requestedValue:[NSNumber numberWithDouble:((double)30/(double)3)]] floatValue], true, [sightConfigId longLongValue]] UTF8String], nil, nil, nil);
            sqlite3_exec([[SQLiteSingleton getInstance] getDatabase], [[NSString stringWithFormat:@"INSERT INTO sight_marks(distance, mark, smaller_units, sight_config_id) VALUES ('%i', '%f', '%i', '%lli');", 25, [[Regression powerRegressWithData:valueDict requestedValue:[NSNumber numberWithDouble:((double)25/(double)3)]] floatValue], true, [sightConfigId longLongValue]] UTF8String], nil, nil, nil);
            sqlite3_exec([[SQLiteSingleton getInstance] getDatabase], [[NSString stringWithFormat:@"INSERT INTO sight_marks(distance, mark, smaller_units, sight_config_id) VALUES ('%i', '%f', '%i', '%lli');", 20, [[Regression powerRegressWithData:valueDict requestedValue:[NSNumber numberWithDouble:((double)20/(double)3)]] floatValue], true, [sightConfigId longLongValue]] UTF8String], nil, nil, nil);
        }
        [self.navigationController popViewControllerAnimated:true];
    }
}

- (IBAction)onCancelClick:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

- (IBAction)onStepperClick:(UIStepper *)sender forEvent:(UIEvent *)event {
    [incrementTextField setText:[[NSNumber numberWithDouble:sender.value] stringValue]];
}

- (BOOL) checkUserData {
    long diffrence = 0;
    
    if (distance1TextField.text.integerValue < distance2TextField.text.integerValue) {
        diffrence = distance2TextField.text.integerValue - distance1TextField.text.integerValue;
    } else {
        diffrence = distance1TextField.text.integerValue - distance2TextField.text.integerValue;
    }
    
    if (diffrence < 20) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please enter two distances that are at least 20 meters/yards apart.\nNumber below that will give bad estimates." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        return false;
    } else {
        return true;
    }
}

@end
