//
//  Regression.h
//  Archery Tools
//
//  Created by Andrew Querol on 3/21/12.
//  Copyright (c) 2012 Cerios Game Studios. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Regression : NSObject

+(NSDecimalNumber *) powerRegressWithData: (NSMutableDictionary *) data requestedValue:(NSNumber *) requestedValue;

@end
