//
//  BowMigrationPolicy.m
//  Archery Tools
//
//  Created by Andrew Querol on 5/19/14.
//  Copyright (c) 2014 Cerios Game Studios. All rights reserved.
//

#import "BowMigrationPolicy.h"
#import "NSManagedObjectModel+KCOrderedAccessorFix.h"

@implementation BowMigrationPolicy

- (BOOL)createDestinationInstancesForSourceInstance:(NSManagedObject *)sInstance entityMapping:(NSEntityMapping *)mapping manager:(NSMigrationManager *)manager error:(NSError *__autoreleasing *)error {
    
    if (![manager userInfo] || ![[manager userInfo] objectForKey:@"bow"]) {
        NSEntityDescription *bowDisc = [NSEntityDescription entityForName:@"Bow" inManagedObjectContext:[manager destinationContext]];
        [manager.destinationModel kc_generateOrderedSetAccessorsForEntity:bowDisc];
        NSEntityDescription *sightConfigDisc = [NSEntityDescription entityForName:@"SightConfig" inManagedObjectContext:[manager destinationContext]];
        [manager.destinationModel kc_generateOrderedSetAccessorsForEntity:sightConfigDisc];
        
        NSManagedObject *newBow = [NSEntityDescription insertNewObjectForEntityForName:@"Bow" inManagedObjectContext:[manager destinationContext]];
        
        [newBow setValue:@"Old Bow" forKey:@"name"];
        [manager setUserInfo:[@{@"bow":newBow} mutableCopy]];
    }
    
    NSString *entityName = [[sInstance entity] name];
    NSManagedObject *newBow = [manager.userInfo objectForKey:@"bow"];
    
    if ([entityName isEqualToString:@"Bow"]) {
        for (NSManagedObject *measurment in [sInstance mutableSetValueForKey:@"settings"]) {
            NSManagedObject *newMeasurment = [NSEntityDescription insertNewObjectForEntityForName:@"Measurement" inManagedObjectContext:[manager destinationContext]];
            
            [newMeasurment setValue:[measurment valueForKey:@"name"] forKey:@"name"];
            [newMeasurment setValue:[measurment valueForKey:@"setting"] forKey:@"setting"];
            [newMeasurment setValue:newBow forKey:@"parent"];
            [[newBow mutableSetValueForKey:@"measurements"] addObject:newMeasurment];
        }
    } else if ([entityName isEqualToString:@"SightMarks"]) {
        NSManagedObject *newSightConfig = [NSEntityDescription insertNewObjectForEntityForName:@"SightConfig" inManagedObjectContext:[manager destinationContext]];
        
        [newSightConfig setValue:[sInstance valueForKey:@"name"] forKey:@"name"];
        // Hack because of the previous versions stored the bool inversly
        [newSightConfig setValue:[NSNumber numberWithBool:![[sInstance valueForKey:@"meters"] boolValue]] forKey:@"metric"];
        [newSightConfig setValue:newBow forKey:@"parent"];
        
        for (NSManagedObject *sightMark in [sInstance mutableSetValueForKey:@"sightMark"]) {
            NSManagedObject *newSightMark = [NSEntityDescription insertNewObjectForEntityForName:@"SightMark" inManagedObjectContext:[manager destinationContext]];
            
            [newSightMark setValue:[sightMark valueForKey:@"distance"] forKey:@"distance"];
            [newSightMark setValue:[sightMark valueForKey:@"mark"] forKey:@"mark"];
            [newSightMark setValue:newSightConfig forKey:@"parent"];
            [[newSightConfig mutableSetValueForKey:@"sightMarks"] addObject:newSightMark];
        }
        [[newBow mutableSetValueForKey:@"sightConfigs"] addObject:newSightConfig];
    } else if ([entityName isEqualToString:@"Note"]) {
        NSManagedObject *newNote = [NSEntityDescription insertNewObjectForEntityForName:@"Note" inManagedObjectContext:[manager destinationContext]];
        [newNote setValue:[sInstance valueForKey:@"data"] forKey:@"text"];
        [newNote setValue:[sInstance valueForKey:@"name"] forKey:@"title"];
        [newNote setValue:newBow forKey:@"parent"];
        [[newBow mutableSetValueForKey:@"notes"] addObject:newNote];
    }
    return [[manager destinationContext] save:error];
}

@end