//
//  KeyDOM.m
//  Archery Tools
//
//  Created by Andrew Querol on 5/16/14.
//  Copyright (c) 2014 Cerios Game Studios. All rights reserved.
//

#import "KeyDOM.h"

@implementation KeyDOM

@synthesize imageFile;
@synthesize keyRank;
@synthesize keyText;
@synthesize keyValue;
@synthesize textColor;

@end
