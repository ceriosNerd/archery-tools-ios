//
//  EditPopoverViewController.m
//  Archery Tools
//
//  Created by Andrew Querol on 1/6/15.
//  Copyright (c) 2015 Cerios Game Studios. All rights reserved.
//

#import "EditPopoverViewController.h"
#import "EventsViewController.h"
#import "SQLiteSingleton.h"

@implementation EditPopoverViewController {
    uint64_t scoreCardId;
}

@synthesize eventDatePicker;
@synthesize eventNameTextField;

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    sqlite3_stmt *cardStatement;
    NSString *cardQuery = [NSString stringWithFormat:@"SELECT name, eventDate FROM score_cards WHERE `id`='%llu'", scoreCardId];
    sqlite3_prepare_v2([[SQLiteSingleton getInstance] getDatabase], [cardQuery UTF8String], -1, &cardStatement, nil);
    if (sqlite3_step(cardStatement) != SQLITE_ROW)
        return;
    [eventNameTextField setText:[NSString stringWithUTF8String:(const char*)sqlite3_column_text(cardStatement, 0)]];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:sqlite3_column_double(cardStatement, 1)];
    sqlite3_finalize(cardStatement);
    [eventDatePicker setDate:date];
}

- (IBAction)onDoneClick:(id)sender {
    NSString *updateScoreCardQuery = [NSString stringWithFormat:@"UPDATE score_cards SET `name`='%@', `eventDate`='%ull' WHERE id=%llu", [eventNameTextField text], (unsigned int)[[eventDatePicker date] timeIntervalSince1970], scoreCardId];
    sqlite3_exec([[SQLiteSingleton getInstance] getDatabase], [updateScoreCardQuery UTF8String], nil, nil, nil);
    [((EventsViewController *)self.popoverPresentationController.delegate) popoverClosing];
    [self dismissViewControllerAnimated:true completion:nil];
}

- (void)setScoreCardId:(uint64_t)cardId {
    scoreCardId = cardId;
}

- (CGSize)preferredContentSize {
    return CGSizeMake(300, 325);
}

@end
