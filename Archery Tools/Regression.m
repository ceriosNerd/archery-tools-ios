//
//  Regression.m
//  Archery Tools
//
//  Created by Andrew Querol on 3/21/12.
//  Copyright (c) 2012 Cerios Game Studios. All rights reserved.
//

#import "Regression.h"

@implementation Regression

+(NSDecimalNumber *) powerRegressWithData: (NSMutableDictionary *) data requestedValue:(NSNumber *) requestedValue {
    double P = 0;
    double Q = 0;
    double R = 0;
    double S = 0;
    double N = [data count];

    double a = 0;
    double b = 0;

    for(NSNumber *x in [data allKeys]) {
        P += log([x doubleValue]);
    }
    
    for(NSNumber *y in [data allValues]) {
        Q += log([y doubleValue]);
    }
    
    for(NSNumber *x in [data allKeys]) {
        R += pow(log([x doubleValue]), 2);
    }
    
    for(NSNumber *x in [data allKeys]) {
        NSNumber *y = [data objectForKey:x];
        S += log([x doubleValue])*log([y doubleValue]);
    }
    
    b = ((N*S) - (P*Q))/((N*R)-pow(P, 2));
    a = exp((Q-(b*P))/N);
    
    return [[NSDecimalNumber alloc] initWithDouble:a*pow([requestedValue doubleValue],b)];
}

@end

