//
//  ScoreViewController.m
//  Archery Tools
//
//  Created by Andrew Querol on 3/27/12.
//  Copyright (c) 2012 Cerios Game Studios. All rights reserved.
//

#import "ScoreViewController.h"

#import "AppDelegate.h"
#import "ArcheryTextLabel.h"
#import "ScoreFooterCell.h"
#import "ScoreHeaderCell.h"
#import "EndInfoViewController.h"
#import "KeyboardGenerator.h"
#import "SQLiteSingleton.h"

@implementation ScoreViewController {
    int scoreCardId;
}

@synthesize generator;
@synthesize keyboard;

- (void)viewDidLoad {
    self.edgesForExtendedLayout = UIRectEdgeNone;

    sqlite3_stmt *keyboardFileStatemet;
    NSString *keyboardFileQuery = [NSString stringWithFormat:@"SELECT keyboard FROM score_cards WHERE `id`='%d'", scoreCardId];
    sqlite3_prepare_v2([[SQLiteSingleton getInstance] getDatabase], [keyboardFileQuery UTF8String], -1, &keyboardFileStatemet, nil);
    
    if (sqlite3_step(keyboardFileStatemet) != SQLITE_ROW) {
        NSLog(@"Got an invalid ScoreCard ID of: %d!", scoreCardId);
        return;
    }
    
    generator = [[KeyboardGenerator alloc] init];
    KeyboardDOM *keyboardDOM = [generator generateKeyboardFromFile:[NSString stringWithUTF8String:(const char*)sqlite3_column_text(keyboardFileStatemet, 0)]];
    keyboard = [generator generateKeyboardViewFromDOM:keyboardDOM];
    
    sqlite3_finalize(keyboardFileStatemet);
    
    [[self tableView] registerNib:[UINib nibWithNibName:@"ScoreCell1" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"scoreCell1"];
    [[self tableView] registerNib:[UINib nibWithNibName:@"ScoreCell2" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"scoreCell2"];
    [[self tableView] registerClass:[ScoreFooterCell class] forHeaderFooterViewReuseIdentifier:@"footerCell"];
    [[self tableView] registerClass:[ScoreHeaderCell class] forHeaderFooterViewReuseIdentifier:@"headerCell"];
    
    [super viewDidLoad];
}

- (void) setScoreCardId:(int) cardId {
    scoreCardId = cardId;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UITableViewHeaderFooterView *view = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"headerCell"];
    return view;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    ScoreFooterCell *footer = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"footerCell"];
    [footer setFrame:CGRectMake(0, 0, [tableView frame].size.width, 22)];
    [footer setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
    
    if (footer) {
        sqlite3_stmt *footerInfoStatement;
        NSString *footerInfoQuery = [NSString stringWithFormat:@"SELECT total, average_score, total_shot, hits FROM rounds WHERE `score_card_id`='%d' ORDER BY number ASC LIMIT 1 OFFSET %ld", scoreCardId, (long)section];
        
        sqlite3_prepare_v2([[SQLiteSingleton getInstance] getDatabase], [footerInfoQuery UTF8String], -1, &footerInfoStatement, nil);
        if (sqlite3_step(footerInfoStatement) != SQLITE_ROW) {
            NSLog(@"Failed to find round number: %ld for score card: %d", (long)section, scoreCardId);
            return nil;
        }
        
        [footer.hitsTextField setText:[NSString stringWithFormat:@"Hits: %d/%d", sqlite3_column_int(footerInfoStatement, 3), sqlite3_column_int(footerInfoStatement, 2)]];
        [footer.avgTextField setText:[NSString stringWithFormat:@"Avg: %.1f", sqlite3_column_double(footerInfoStatement, 1)]];
        [footer.totalTextField setText:[NSString stringWithFormat:@"%d", sqlite3_column_int(footerInfoStatement, 0)]];
        
        sqlite3_finalize(footerInfoStatement);
    }
    
    return footer;
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [self.tableView reloadData];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 33;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 22;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    sqlite3_stmt *sectionsStatement;
    NSString *sectionsQuery = [NSString stringWithFormat:@"SELECT COUNT(*) FROM rounds WHERE `score_card_id`='%d'", scoreCardId];
    
    sqlite3_prepare_v2([[SQLiteSingleton getInstance] getDatabase], [sectionsQuery UTF8String], -1, &sectionsStatement, nil);
    if (sqlite3_step(sectionsStatement) != SQLITE_ROW) {
        NSLog(@"Failed to count number of rounds for score card: %d", scoreCardId);
        return 0;
    }
    int count = sqlite3_column_int(sectionsStatement, 0);
    sqlite3_finalize(sectionsStatement);
    
    return count;
}

- (NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section {
    sqlite3_stmt *rowsStatement;
    NSString *rowsQuery = [NSString stringWithFormat:@"SELECT ends_per_round FROM rounds WHERE `score_card_id`='%d' ORDER BY number ASC LIMIT 1 OFFSET %ld", scoreCardId, (long) section];
    
    sqlite3_prepare_v2([[SQLiteSingleton getInstance] getDatabase], [rowsQuery UTF8String], -1, &rowsStatement, nil);
    if (sqlite3_step(rowsStatement) != SQLITE_ROW) {
        NSLog(@"Failed to retreive the number of ends per round for score card: %d", scoreCardId);
        return 0;
    }
    int rows = sqlite3_column_int(rowsStatement, 0);
    sqlite3_finalize(rowsStatement);
    
    return rows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = nil;
    
    sqlite3_stmt *arrowsPerEndStatement;
    NSString *arrowsPerEndQuery = [NSString stringWithFormat:@"SELECT arrows_per_end, id FROM rounds WHERE `score_card_id`='%d' ORDER BY number ASC LIMIT 1 OFFSET %ld", scoreCardId, (long) indexPath.section];
    
    sqlite3_prepare_v2([[SQLiteSingleton getInstance] getDatabase], [arrowsPerEndQuery UTF8String], -1, &arrowsPerEndStatement, nil);
    if (sqlite3_step(arrowsPerEndStatement) != SQLITE_ROW) {
        NSLog(@"Failed to retreive the number of ends per round for score card: %d", scoreCardId);
        return 0;
    }
    int arrowsPerEnd = sqlite3_column_int(arrowsPerEndStatement, 0);
    int roundId = sqlite3_column_int(arrowsPerEndStatement, 1);
    sqlite3_finalize(arrowsPerEndStatement);
    
    if (arrowsPerEnd <= 6) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"scoreCell1"];
    } else if (arrowsPerEnd > 6 && arrowsPerEnd <= 12) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"scoreCell2"];
    }

    if ([cell isKindOfClass:[ScoreEndCell class]]) {
        sqlite3_stmt *endIdStatement;
        NSString *endIdQuery = [NSString stringWithFormat:@"SELECT id FROM ends WHERE `round_id`='%d' ORDER BY number ASC LIMIT 1 OFFSET %ld", roundId, (long) indexPath.row];
        
        sqlite3_prepare_v2([[SQLiteSingleton getInstance] getDatabase], [endIdQuery UTF8String], -1, &endIdStatement, nil);
        if (sqlite3_step(endIdStatement) != SQLITE_ROW) {
            NSLog(@"Failed to retreive the end id for round ID: %d", roundId);
            return 0;
        }
        int endId = sqlite3_column_int(endIdStatement, 0);
        sqlite3_finalize(endIdStatement);
        
        [(ScoreEndCell*)cell setDelgate:self];
        [(ScoreEndCell*)cell setTableView:[self tableView]];
        [(ScoreEndCell*)cell setEndId:endId];
        [[(ScoreEndCell*)cell endNumberTextLabel] setText:[NSString stringWithFormat:@"%i:", (int)indexPath.row + 1]];
        [(ScoreEndCell*)cell setNumberOfArrows:arrowsPerEnd];
        [(ScoreEndCell*)cell setKeyboard:keyboard];
        [(ScoreEndCell*)cell initCell];
    }
    
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (void)updateLabels:(NSIndexPath *)index {
    sqlite3_stmt *infoStatement;
    NSString *footerInfoQuery = [NSString stringWithFormat:@"SELECT total, average_score, total_shot, hits, ends_per_round FROM rounds WHERE `score_card_id`='%d' ORDER BY number ASC LIMIT 1 OFFSET %ld", scoreCardId, (long)index.section];
    
    sqlite3_prepare_v2([[SQLiteSingleton getInstance] getDatabase], [footerInfoQuery UTF8String], -1, &infoStatement, nil);
    if (sqlite3_step(infoStatement) != SQLITE_ROW) {
        NSLog(@"Failed to find round number: %ld for score card: %d", (long) index.section, scoreCardId);
        return;
    }
    
    for (long i = index.row; i < sqlite3_column_int(infoStatement, 4); i++) {
        NSIndexPath *newIndex = [NSIndexPath indexPathForRow:i inSection:index.section];
        [self configureCell:[self.tableView cellForRowAtIndexPath:newIndex] atIndexPath:newIndex];
    }
    
    if ([[self tableView] footerViewForSection:index.section]) {
        ScoreFooterCell *customCell = (ScoreFooterCell *)[[self tableView] footerViewForSection:index.section];
        [[customCell totalTextField] setText:[NSString stringWithFormat:@"%d", sqlite3_column_int(infoStatement, 0)]];
        [[customCell hitsTextField] setText:[NSString stringWithFormat:@"Hits: %i/%i", sqlite3_column_int(infoStatement, 3), sqlite3_column_int(infoStatement, 2)]];
        [[customCell avgTextField] setText:[NSString stringWithFormat:@"Avg: %.1f", sqlite3_column_double(infoStatement, 1)]];
    }
    sqlite3_finalize(infoStatement);
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    [self configureCell:cell atIndexPath:indexPath];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return  [NSString stringWithFormat:@"Round: %i", ((section + 1) > UINT32_MAX) ? UINT32_MAX : (u_int32_t)section + 1]; // Ugly Cast
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    if ([cell isKindOfClass:[ScoreEndCell class]]) {
        ScoreEndCell *scoreCell = (ScoreEndCell *) cell;
        
        sqlite3_stmt *endStatement;
        NSString *endQuery = [NSString stringWithFormat:@"SELECT running_total, total, hits FROM ends WHERE `id`='%d' LIMIT 1", [scoreCell getEndId]];
        
        sqlite3_prepare_v2([[SQLiteSingleton getInstance] getDatabase], [endQuery UTF8String], -1, &endStatement, nil);
        if (sqlite3_step(endStatement) != SQLITE_ROW) {
            NSLog(@"Failed to retreive the end id for end ID: %d", [scoreCell getEndId]);
            return;
        }
        
        [scoreCell setRunningTotal:[NSString stringWithFormat:@"%d", sqlite3_column_int(endStatement, 0)]];
        [scoreCell setEndTotal:[NSString stringWithFormat:@"%d", sqlite3_column_int(endStatement, 1)]];
        [scoreCell setHits:[NSString stringWithFormat:@"%d", sqlite3_column_int(endStatement, 2)]];
        sqlite3_finalize(endStatement);
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    sqlite3_stmt *arrowsPerEndStatement;
    NSString *arrowsPerEndQuery = [NSString stringWithFormat:@"SELECT arrows_per_end FROM rounds WHERE `score_card_id`='%d' ORDER BY number ASC LIMIT 1 OFFSET %li", scoreCardId, (long) indexPath.section];
    
    sqlite3_prepare_v2([[SQLiteSingleton getInstance] getDatabase], [arrowsPerEndQuery UTF8String], -1, &arrowsPerEndStatement, nil);
    if (sqlite3_step(arrowsPerEndStatement) != SQLITE_ROW) {
        NSLog(@"Failed to retreive the arrows per end for round ID: %li", (long) indexPath.section);
        return 44;
    }
    int arrowsPerEnd = sqlite3_column_int(arrowsPerEndStatement, 0);
    sqlite3_finalize(arrowsPerEndStatement);
    
    if (arrowsPerEnd <= 6) {
        return 34;
    } else if (arrowsPerEnd > 6 && arrowsPerEnd <= 12) {
        return 66;
    }
    return 44;
}

- (void)infoPush:(int) sender {
    [self performSegueWithIdentifier:@"infoPush" sender:[NSNumber numberWithInt:sender]];
}

@end