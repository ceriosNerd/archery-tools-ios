//
//  EventTypeViewController.h
//  Archery Tools
//
//  Created by Andrew Querol on 3/26/12.
//  Copyright (c) 2012 Cerios Game Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@class EventType, EventDOM, RoundDOM;

@interface EventTypeViewController : UIViewController <UIPickerViewDelegate, UIPickerViewDataSource, NSXMLParserDelegate, UIAlertViewDelegate>

@property NSMutableArray *categories;
@property NSMutableArray *currentEvents;

@property EventDOM *currentEvent;
@property RoundDOM *currentRound;

@property NSString *currentKeyboard;
@property NSString *currentCategoryName;
@property NSString *currentSubtypeString;
@property NSMutableString *currentElementText;

@property Boolean xmlLoaded;
@property NSXMLParser *xmlParser;

@property (weak, nonatomic) IBOutlet UIPickerView *eventTypePicker;

- (IBAction)onCreateClick:(id)sender;
- (IBAction)onPracticeClick:(id)sender;

@end
