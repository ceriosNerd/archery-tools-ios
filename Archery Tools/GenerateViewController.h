//
//  GenerateViewController.h
//  Archery Tools
//
//  Created by Andrew Querol on 3/24/12.
//  Copyright (c) 2012 Cerios Game Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GenerateViewController : UITableViewController

@property (weak, nonatomic) IBOutlet UITextField *distance1TextField;
@property (weak, nonatomic) IBOutlet UITextField *distance2TextField;
@property (weak, nonatomic) IBOutlet UITextField *distance3TextField;
@property (weak, nonatomic) IBOutlet UITextField *sightMark1TextField;
@property (weak, nonatomic) IBOutlet UITextField *sightMark2TextField;
@property (weak, nonatomic) IBOutlet UITextField *sightMark3TextField;
@property (weak, nonatomic) IBOutlet UILabel *meterYardLabel;
@property (weak, nonatomic) IBOutlet UITextField *incrementTextField;
@property (weak, nonatomic) IBOutlet UIStepper *stepper;
@property (weak, nonatomic) IBOutlet UISwitch *bunnyMarksSwitch;
@property (weak, nonatomic) IBOutlet UILabel *bunnyMarksLabel;

@property (strong) NSNumber *sightConfigId;

- (IBAction)onDoneClick:(id)sender;
- (IBAction)onCancelClick:(id)sender;
- (IBAction)onStepperClick:(UIStepper *)sender forEvent:(UIEvent *)event;

@end
