//
//  MeasurementsViewController.m
//  Archery Tools
//
//  Created by Andrew Querol on 5/17/14.
//  Copyright (c) 2014 Cerios Game Studios. All rights reserved.
//

#import "MeasurementsViewController.h"
#import "SQLiteSingleton.h"

@implementation MeasurementsViewController

@synthesize bowId;

- (NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section {
    sqlite3_stmt *measurementsStatement;
    NSString *measurementsQuery = [NSString stringWithFormat:@"SELECT COUNT(*) FROM measurements WHERE `bow_id`='%lli'", [bowId longLongValue]];
    sqlite3_prepare_v2([[SQLiteSingleton getInstance] getDatabase], [measurementsQuery UTF8String], -1, &measurementsStatement, nil);
    
    int count = 0;
    if (sqlite3_step(measurementsStatement) == SQLITE_ROW)
        count = sqlite3_column_int(measurementsStatement, 0);
    sqlite3_finalize(measurementsStatement);
    
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"settings"];
    if (!cell)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"settings"];
    
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return true;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        sqlite3_exec([[SQLiteSingleton getInstance] getDatabase], [[NSString stringWithFormat:@"DELETE FROM measurements WHERE `id`=(SELECT id FROM notes WHERE `bow_id`='%lli' ORDER BY id ASC LIMIT 1 OFFSET %ld)", [bowId longLongValue], (long)[indexPath row]] UTF8String], nil, nil, nil);
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    sqlite3_stmt *measurementStatement;
    NSString *measurementQuery = [NSString stringWithFormat:@"SELECT name, measurement, id FROM measurements WHERE `bow_id`='%lli' ORDER BY id ASC LIMIT 1 OFFSET %li", [bowId longLongValue], (long) indexPath.row];
    sqlite3_prepare_v2([[SQLiteSingleton getInstance] getDatabase], [measurementQuery UTF8String], -1, &measurementStatement, nil);
    
    if (sqlite3_step(measurementStatement) != SQLITE_ROW) {
        NSLog(@"Failed to retreive the measurement at: %li", (long) indexPath.row);
        return;
    }
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Edit Measurement" message:nil delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Done", nil];
    [alert setDelegate:self];
    [alert setAlertViewStyle:UIAlertViewStyleLoginAndPasswordInput];
    [[alert textFieldAtIndex:0] setText:[NSString stringWithUTF8String:(const char *) sqlite3_column_text(measurementStatement, 0)]];
    [[alert textFieldAtIndex:0] setAutocapitalizationType:UITextAutocapitalizationTypeWords];
    [[alert textFieldAtIndex:1] setText:[[NSNumber numberWithDouble:sqlite3_column_double(measurementStatement, 1)] stringValue]];
    [[alert textFieldAtIndex:1] setKeyboardType:UIKeyboardTypeDecimalPad];
    [[alert textFieldAtIndex:1] setSecureTextEntry:false];
    [alert.layer setValue:[NSNumber numberWithLongLong:sqlite3_column_int64(measurementStatement, 2)] forKey:@"edit"];
    sqlite3_finalize(measurementStatement);
    [alert show];
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    sqlite3_stmt *measurementStatement;
    NSString *measurementQuery = [NSString stringWithFormat:@"SELECT name, measurement FROM measurements WHERE `bow_id`='%lli' ORDER BY id ASC LIMIT 1 OFFSET %li", [bowId longLongValue], (long) indexPath.row];
    sqlite3_prepare_v2([[SQLiteSingleton getInstance] getDatabase], [measurementQuery UTF8String], -1, &measurementStatement, nil);
    
    if (sqlite3_step(measurementStatement) != SQLITE_ROW) {
        NSLog(@"Failed to retreive the measurement at: %li", (long) indexPath.row);
        return;
    }
    cell.textLabel.text = [NSString stringWithUTF8String:(const char *) sqlite3_column_text(measurementStatement, 0)];
    [cell.detailTextLabel setText:[[NSNumber numberWithDouble:sqlite3_column_double(measurementStatement, 1)] stringValue]];
    sqlite3_finalize(measurementStatement);
}

- (IBAction)addButton:(id)sender {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Enter the measurement you want to store" message:nil delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Create", nil];
    [alert setDelegate:self];
    [alert setAlertViewStyle:UIAlertViewStyleLoginAndPasswordInput];
    [[alert textFieldAtIndex:0] setPlaceholder:@"Measurement Name"];
    [[alert textFieldAtIndex:0] setAutocapitalizationType:UITextAutocapitalizationTypeWords];
    [[alert textFieldAtIndex:1] setPlaceholder:@"Measurement"];
    [[alert textFieldAtIndex:1] setKeyboardType:UIKeyboardTypeDecimalPad];
    [[alert textFieldAtIndex:1] setSecureTextEntry:false];
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex != [alertView cancelButtonIndex]) {
        if ([alertView.layer valueForKey:@"edit"]) {
            long long editId = [(NSNumber *)[alertView.layer valueForKey:@"edit"] longLongValue];
            sqlite3_exec([[SQLiteSingleton getInstance] getDatabase], [[NSString stringWithFormat:@"UPDATE measurements SET `name`='%@', `measurement`='%f', `synced`='0' WHERE `id`=%lli", [[alertView textFieldAtIndex:0] text], [[NSDecimalNumber decimalNumberWithString:[[alertView textFieldAtIndex:1] text]] floatValue], editId] UTF8String], nil, nil, nil);
        } else {
            sqlite3_exec([[SQLiteSingleton getInstance] getDatabase], [[NSString stringWithFormat:@"INSERT INTO measurements(name, measurement, bow_id) VALUES ('%@', '%f', '%lli');", [[alertView textFieldAtIndex:0] text], [[NSDecimalNumber decimalNumberWithString:[[alertView textFieldAtIndex:1] text]] floatValue], [bowId longLongValue]] UTF8String], nil, nil, nil);
        }
    }
    [self.tableView reloadData];
}

- (BOOL)alertViewShouldEnableFirstOtherButton:(UIAlertView *)alertView {
    NSString *inputText = [[alertView textFieldAtIndex:0] text];
    if([inputText length] > 0 && [NSDecimalNumber decimalNumberWithString:[[alertView textFieldAtIndex:1] text]] != nil) {
        return YES;
    } else {
        return NO;
    }
}

@end
