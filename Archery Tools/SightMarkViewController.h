//
//  SightMarkViewController.h
//  Archery Tools
//
//  Created by Andrew Querol on 3/24/12.
//  Copyright (c) 2012 Cerios Game Studios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BowPageViewController.h"

@interface SightMarkViewController : UITableViewController <UIAlertViewDelegate>

@property NSNumber *sightConfigId;

- (IBAction)addClicked:(id)sender;

@end
