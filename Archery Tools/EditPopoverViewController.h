//
//  EditPopoverViewController.h
//  Archery Tools
//
//  Created by Andrew Querol on 1/6/15.
//  Copyright (c) 2015 Cerios Game Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EditPopoverViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *eventNameTextField;
@property (weak, nonatomic) IBOutlet UIDatePicker *eventDatePicker;

- (IBAction)onDoneClick:(id)sender;
- (void) setScoreCardId:(uint64_t) cardId;
@end
