//
//  ScoreFooterCell.h
//  Archery Tools
//
//  Created by Andrew Querol on 3/28/12.
//  Copyright (c) 2012 Cerios Game Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScoreFooterCell : UITableViewHeaderFooterView

@property UILabel *avgTextField;
@property UILabel *totalTextField;
@property UILabel *hitsTextField;

@end
