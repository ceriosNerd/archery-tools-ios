//
//  NoteEditCreateViewController.h
//  Archery Tools
//
//  Created by Andrew Querol on 3/25/12.
//  Copyright (c) 2012 Cerios Game Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NoteEditCreateViewController : UITableViewController

@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextView *notesTextView;
@property (strong) NSNumber *noteId;
@property (strong) NSNumber *bowId;

- (IBAction)onDoneClick:(id)sender;

@end
