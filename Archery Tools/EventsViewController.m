//
//  EventsViewController.m
//  Archery Tools
//
//  Created by Andrew Querol on 3/25/12.
//  Copyright (c) 2012 Cerios Game Studios. All rights reserved.
//

#import "EventsViewController.h"
#import "AppDelegate.h"
#import "ScoreViewController.h"
#import "SQLiteSingleton.h"
#import "SavedEventsCell.h"
#import "EditPopoverViewController.h"

@implementation EventsViewController {
    NSDateFormatter *dateFormatter;
    EditPopoverViewController *editViewController;
}

- (void)viewDidLoad {
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    [dateFormatter setTimeStyle:NSDateFormatterNoStyle];
    
    editViewController = [[[NSBundle mainBundle] loadNibNamed:@"editEventPopover" owner:self options:nil] objectAtIndex:0];
    editViewController.modalInPopover = true;
    editViewController.modalPresentationStyle = UIModalPresentationPopover;
}

- (NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section {
    sqlite3_stmt *countStatement;
    NSString *countQuery = @"SELECT COUNT(*) FROM score_cards";
    sqlite3_prepare_v2([[SQLiteSingleton getInstance] getDatabase], [countQuery UTF8String], -1, &countStatement, nil);
    
    int count = 0;
    if (sqlite3_step(countStatement) == SQLITE_ROW)
        count = sqlite3_column_int(countStatement, 0);
    sqlite3_finalize(countStatement);
    
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SavedEventsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"event"];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return true;
}

- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewRowAction *moreAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Edit" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        sqlite3_stmt *idStatement;
        NSString *idQuery = [NSString stringWithFormat:@"SELECT id FROM score_cards ORDER BY id ASC LIMIT 1 OFFSET %ld", (long)[indexPath row]];
        sqlite3_prepare_v2([[SQLiteSingleton getInstance] getDatabase], [idQuery UTF8String], -1, &idStatement, nil);
        if (sqlite3_step(idStatement) != SQLITE_ROW)
            return;
        uint64_t cardId = sqlite3_column_int64(idStatement, 0);
        sqlite3_finalize(idStatement);
        
        [editViewController setScoreCardId:cardId];
        UIPopoverPresentationController *controller = [editViewController popoverPresentationController];
        controller.sourceView = [tableView cellForRowAtIndexPath:indexPath];
        controller.sourceRect = [tableView cellForRowAtIndexPath:indexPath].bounds;
        controller.permittedArrowDirections = UIPopoverArrowDirectionAny;
        controller.delegate = self;

        [self presentViewController:editViewController animated:true completion:nil];
        [self.tableView setEditing:NO];
    }];
    moreAction.backgroundColor = [UIColor lightGrayColor];

    UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive title:@"Delete"  handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
        sqlite3_exec([[SQLiteSingleton getInstance] getDatabase], [[NSString stringWithFormat:@"DELETE FROM score_cards WHERE `id`=(SELECT id FROM score_cards ORDER BY id ASC LIMIT 1 OFFSET %ld)", (long)[indexPath row]] UTF8String], nil, nil, nil);
        [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    }];
    
    return @[deleteAction, moreAction];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView reloadData];
}

- (void)configureCell:(SavedEventsCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    sqlite3_stmt *cardStatement;
    NSString *cardQuery = [NSString stringWithFormat:@"SELECT name, eventDate FROM score_cards ORDER BY id ASC LIMIT 1 OFFSET %ld", (long)[indexPath row]];
    sqlite3_prepare_v2([[SQLiteSingleton getInstance] getDatabase], [cardQuery UTF8String], -1, &cardStatement, nil);
    if (sqlite3_step(cardStatement) != SQLITE_ROW)
        return;
    [cell.titleTextField setText:[NSString stringWithUTF8String:(const char*)sqlite3_column_text(cardStatement, 0)]];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:sqlite3_column_double(cardStatement, 1)];
    sqlite3_finalize(cardStatement);
    [cell.dateTextField setText:[dateFormatter stringFromDate:date]];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.destinationViewController class] == [ScoreViewController class]) {
        sqlite3_stmt *cardStatement;
        NSString *cardQuery = [NSString stringWithFormat:@"SELECT id FROM score_cards ORDER BY id ASC LIMIT 1 OFFSET %ld", (long)[[self.tableView indexPathForSelectedRow] row]];
        sqlite3_prepare_v2([[SQLiteSingleton getInstance] getDatabase], [cardQuery UTF8String], -1, &cardStatement, nil);
        if (sqlite3_step(cardStatement) != SQLITE_ROW)
            return;
        
        ScoreViewController *controller = segue.destinationViewController;
        [controller setScoreCardId:sqlite3_column_int(cardStatement, 0)];
        sqlite3_finalize(cardStatement);
    }
}

- (UIModalPresentationStyle)adaptivePresentationStyleForPresentationController:(UIPresentationController *)controller {
    return UIModalPresentationNone;
}

- (void)popoverClosing {
    [self.tableView reloadData];
}

- (void)popoverPresentationControllerDidDismissPopover:(UIPopoverPresentationController *)popoverPresentationController {
    [self.tableView reloadData];
}

@end
