//
//  EventVersion1ToVersion2.m
//  Archery Tools
//
//  Created by Andrew Querol on 4/19/12.
//  Copyright (c) 2012 Cerios Game Studios. All rights reserved.
//

#import "EventVersion1ToVersion2.h"

@implementation EventVersion1ToVersion2

- (BOOL)createRelationshipsForDestinationInstance:(NSManagedObject *)dInstance entityMapping:(NSEntityMapping *)mapping manager:(NSMigrationManager *)manager error:(NSError **)error
{
    NSManagedObject *newEventType = [NSEntityDescription insertNewObjectForEntityForName:@"EventType" inManagedObjectContext:[manager destinationContext]];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Event" inManagedObjectContext:[manager sourceContext]];
    [fetchRequest setEntity:entity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"date = %@", [dInstance valueForKey:@"date"]];
    [fetchRequest setPredicate:predicate];
    
    NSArray *oldEvents = [[manager sourceContext] executeFetchRequest:fetchRequest error:nil];
    
    NSManagedObject *oldEvent = [oldEvents objectAtIndex:0];
    for (int i = 0; i < [[[oldEvent valueForKey:@"eventType"] valueForKey:@"rounds"] integerValue]; i++) {
        NSManagedObject *round = [NSEntityDescription insertNewObjectForEntityForName:@"EventTypeRound"inManagedObjectContext:[manager destinationContext]];
        [round setValue:[[oldEvent valueForKey:@"eventType"] valueForKey:@"arrowsPerEnd"] forKey:@"arrowsPerEnd"];
        [round setValue:[[oldEvent valueForKey:@"eventType"] valueForKey:@"endsPerRound"] forKey:@"endsPerRound"];
        [round setValue:[[oldEvent valueForKey:@"eventType"] valueForKey:@"keyboardType"] forKey:@"keyboardType"];
        [round setValue:newEventType forKey:@"parent"];
        [[newEventType mutableSetValueForKey:@"rounds"] addObject:round];
        
    }
    [dInstance setValue:newEventType forKey:@"eventType"];
    return [super createRelationshipsForDestinationInstance:dInstance entityMapping:mapping manager:manager error:error];
}

@end
