//
//  TabBarControllerHelper.h
//  Archery Tools
//
//  Created by Andrew Querol on 5/20/14.
//  Copyright (c) 2014 Cerios Game Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TabBarControllerHelper : UITabBarController

@end
