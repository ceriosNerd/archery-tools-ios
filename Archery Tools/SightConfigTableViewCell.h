//
//  SightConfigTableViewCell.h
//  Archery Tools
//
//  Created by Andrew Querol on 5/18/14.
//  Copyright (c) 2014 Cerios Game Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SightConfigTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UISegmentedControl *unitsSegmentedControl;
@property NSNumber *sightConfigId;

- (IBAction)onUnitsChanged:(UISegmentedControl *)sender;
@end
