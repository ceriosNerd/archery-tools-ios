//
//  NoteEditCreateViewController.m
//  Archery Tools
//
//  Created by Andrew Querol on 3/25/12.
//  Copyright (c) 2012 Cerios Game Studios. All rights reserved.
//

#import "NoteEditCreateViewController.h"
#import "SQLiteSingleton.h"

@implementation NoteEditCreateViewController

@synthesize nameTextField;
@synthesize notesTextView;
@synthesize noteId;
@synthesize bowId;

static inline char* checkNullString(const unsigned char *string) {
    if (string) {
        return (char *)string;
    }
    return "";
}

- (void)viewDidUnload {
    [self setNameTextField:nil];
    [self setNotesTextView:nil];
    [super viewDidUnload];
}

- (void)viewDidLoad {
    [self.parentViewController setTitle:@"New Note"];
    if (noteId) {
        sqlite3_stmt *noteStatement;
        NSString *noteQuery = [NSString stringWithFormat:@"SELECT title, content FROM notes WHERE `id`='%lli' LIMIT 1", [noteId longLongValue]];
        sqlite3_prepare_v2([[SQLiteSingleton getInstance] getDatabase], [noteQuery UTF8String], -1, &noteStatement, nil);
        
        if (sqlite3_step(noteStatement) != SQLITE_ROW) {
            NSLog(@"Failed to retreive note id: %lli", [noteId longLongValue]);
            return;
        }
        [nameTextField setText:[NSString stringWithUTF8String:checkNullString(sqlite3_column_text(noteStatement, 0))]];
        [notesTextView setText:[NSString stringWithUTF8String:checkNullString(sqlite3_column_text(noteStatement, 1))]];
        sqlite3_finalize(noteStatement);
    } else {
        sqlite3_exec([[SQLiteSingleton getInstance] getDatabase], [[NSString stringWithFormat:@"INSERT INTO notes(bow_id) VALUES(%lli);", [bowId longLongValue]] UTF8String], nil, nil, nil);
        noteId = [NSNumber numberWithLongLong:sqlite3_last_insert_rowid([[SQLiteSingleton getInstance] getDatabase])];
    }
    [super viewDidLoad];
}

- (IBAction)onDoneClick:(id)sender {
    sqlite3_exec([[SQLiteSingleton getInstance] getDatabase], [[NSString stringWithFormat:@"UPDATE notes SET `title`='%@', `content`='%@', `synced`='0' WHERE `id`='%lli'", nameTextField.text, notesTextView.text,[noteId longLongValue]] UTF8String], nil, nil, nil);
    [self.navigationController popViewControllerAnimated:true];
}

@end
