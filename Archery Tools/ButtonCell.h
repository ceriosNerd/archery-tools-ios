//
//  ButtonCell.h
//  Archery Tools
//
//  Created by Andrew Querol on 5/19/14.
//  Copyright (c) 2014 Cerios Game Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ButtonCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIButton *button;
@end
