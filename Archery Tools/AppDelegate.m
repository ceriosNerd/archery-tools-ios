//
//  AppDelegate.m
//  Archery Tools
//
//  Created by Andrew Querol on 3/24/12.
//  Copyright (c) 2012 Cerios Game Studios. All rights reserved.
//

#import "AppDelegate.h"
#import "NSManagedObjectModel+KCOrderedAccessorFix.h"
#import "TabBarControllerHelper.h"
#import <iAd/iAd.h>
#import <SystemConfiguration/SystemConfiguration.h>
#import "SQLiteSingleton.h"
#import <MessageUI/MFMailComposeViewController.h>

@implementation AppDelegate

@synthesize window = _window;
@synthesize managedObjectContext = __managedObjectContext;
@synthesize managedObjectModel = __managedObjectModel;
@synthesize persistentStoreCoordinator = __persistentStoreCoordinator;

static const float VERSION = 1.7;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
#if DEBUG == 1
    NSLog(@"App Directory: %@",[[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject]);
#endif
    if ([self persistentStoreCoordinator] != nil) {
        NSLog(@"Found CoreData SQLite, Migrating to pure SQLite from CoreData store.");
        [[SQLiteSingleton getInstance] tryToMigrateFromCoreData:[self persistentStoreCoordinator] onSuccess:^(SuccessCode code) {
            NSLog(@"Successfully migrated to pure SQLite, purging all old CoreData stores.");
            // Purge old CoreData items
            NSString *docsDirectoryPath = [[AppDelegate applicationDocumentsDirectory] path];
            NSArray *docsDirectoryContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:docsDirectoryPath error:nil];
            for (NSString *docsDirectoryItem in docsDirectoryContents) {
                if ([docsDirectoryItem containsString:@"Archery_Tools"]) {
                    [[NSFileManager defaultManager] removeItemAtURL:[NSURL URLWithString:docsDirectoryItem relativeToURL:[AppDelegate applicationDocumentsDirectory]] error:nil];
                }
            }
        } onFailure:^(FailCode code) {
            if (![MFMailComposeViewController canSendMail]) {
                NSLog(@"Error migrating the CoreData store.");
            }
            
            UIAlertController *reportAlert = [UIAlertController alertControllerWithTitle:@"Error Migrating Data" message:@"There was an error migrating your data to the newest version. Do you want to send and report this error?" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *reportAction = [UIAlertAction actionWithTitle:@"Report" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
                [controller setSubject:@"Error Report"];
                [controller setMessageBody:@"Migration Error Report." isHTML:NO];
                
                NSError *listingError = nil;
                NSArray *directoryContent = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:[[AppDelegate applicationDocumentsDirectory] absoluteString] error:&listingError];
                if (listingError) {
                    NSLog(@"Error getting files to send in email error report.");
                    return;
                }
                
                // Attach all files in the Documents directory (This is where I save all data stores and app data
                for (int i = 0; i < (int)[directoryContent count]; i++) {
                    [controller addAttachmentData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[directoryContent objectAtIndex:i] relativeToURL:[AppDelegate applicationDocumentsDirectory]]] mimeType:@"application/octet-stream" fileName:[directoryContent objectAtIndex:i]];
                }
                
                [controller setModalInPopover:true];
                [[[self window] rootViewController] presentViewController:controller animated:YES completion:nil];
            }];
            UIAlertAction *noAction = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDestructive handler:nil];
            [reportAlert addAction:reportAction];
            [reportAlert addAction:noAction];
            [[[self window] rootViewController] presentViewController:reportAlert animated:true completion:nil];
        }];
    }
    
    // Make sure a images directory exists
    [[NSFileManager defaultManager] createDirectoryAtURL:[[AppDelegate applicationDocumentsDirectory] URLByAppendingPathComponent:@"images"] withIntermediateDirectories:true attributes:nil error:nil];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (![defaults objectForKey:@"firstRun"]) {
        [defaults setObject:[NSDate date] forKey:@"firstRun"];
        [defaults setFloat:VERSION forKey:@"version"];
    } else if ([defaults floatForKey:@"version"] != VERSION) {
        [defaults setObject:[NSNumber numberWithFloat:VERSION] forKey:@"version"];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    application.applicationSupportsShakeToEdit = YES;
    
    return YES;
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Saves changes in the application's managed object context before the application terminates.
    sqlite3_close([[SQLiteSingleton getInstance] getDatabase]);
    
    // Purge old CoreData items again just to make sure everything is gone.
    NSString *docsDirectoryPath = [[AppDelegate applicationDocumentsDirectory] path];
    NSArray *docsDirectoryContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:docsDirectoryPath error:nil];
    for (NSString *docsDirectoryItem in docsDirectoryContents) {
        if ([docsDirectoryItem containsString:@"Archery_Tools"]) {
            [[NSFileManager defaultManager] removeItemAtURL:[NSURL URLWithString:docsDirectoryItem relativeToURL:[AppDelegate applicationDocumentsDirectory]] error:nil];
        }
    }
}

- (void)saveContext {
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        } 
    }
}

#pragma mark - Leagcy Core Data stack (Only here for possible migrations to SQLite)

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel {
    if (__managedObjectModel != nil) {
        return __managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Archery_Tools" withExtension:@"momd"];
    __managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    [__managedObjectModel kc_generateOrderedSetAccessors];
    return __managedObjectModel;
}

- (BOOL)progressivelyMigrateURL:(NSURL*)sourceStoreURL ofType:(NSString*)type toModel:(NSManagedObjectModel*)finalModel error:(NSError**)error {
    NSDictionary *sourceMetadata = [NSPersistentStoreCoordinator metadataForPersistentStoreOfType:type URL:sourceStoreURL error:error];
    if (!sourceMetadata) return NO;
    
    if ([finalModel isConfiguration:nil compatibleWithStoreMetadata:sourceMetadata]) {
        if (error != nil)
            *error = nil;
        return YES;
    }
    //Find the source model
    NSManagedObjectModel *sourceModel = [NSManagedObjectModel mergedModelFromBundles:nil forStoreMetadata:sourceMetadata];
    NSAssert(sourceModel != nil, ([NSString stringWithFormat: @"Failed to find source model\n%@", sourceMetadata]));
    
    //Find all of the mom and momd files in the Resources directory
    NSMutableArray *modelPaths = [NSMutableArray array];
    NSArray *momdArray = [[NSBundle mainBundle] pathsForResourcesOfType:@"momd" inDirectory:nil];
    for (NSString *momdPath in momdArray) {
        NSString *resourceSubpath = [momdPath lastPathComponent];
        NSArray *array = [[NSBundle mainBundle] pathsForResourcesOfType:@"mom" inDirectory:resourceSubpath];
        [modelPaths addObjectsFromArray:array];
    }
    NSArray* otherModels = [[NSBundle mainBundle] pathsForResourcesOfType:@"mom" inDirectory:nil];
    [modelPaths addObjectsFromArray:otherModels];
    
    if (!modelPaths || ![modelPaths count]) {
        // Throw an error if there are no models
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        [dict setValue:@"No models found in bundle" forKey:NSLocalizedDescriptionKey];
        // Populate the error
        if (error != nil)
            *error = [NSError errorWithDomain:@"Cerios" code:8001 userInfo:dict];
        return NO;
    }
    
    // See if we can find a matching destination model
    NSMappingModel *mappingModel = nil;
    NSManagedObjectModel *targetModel = nil;
    NSString *modelPath = nil;
    for (modelPath in modelPaths) {
        targetModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:[NSURL fileURLWithPath:modelPath]];
        mappingModel = [NSMappingModel mappingModelFromBundles:nil forSourceModel:sourceModel destinationModel:targetModel];
        // If we found a mapping model then proceed
        if (mappingModel) break;
        targetModel = nil;
    }
    // We have tested every model, if nil here we failed
    if (!mappingModel) {
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        [dict setValue:@"No models found in bundle" forKey:NSLocalizedDescriptionKey];
        if (error != nil)
            *error = [NSError errorWithDomain:@"Cerios" code:8001 userInfo:dict];
        return NO;
    }
    // We have a mapping model and a destination model.  Time to migrate
    NSMigrationManager *manager = [[NSMigrationManager alloc] initWithSourceModel:sourceModel destinationModel:targetModel];
    
    NSString *modelName = [[modelPath lastPathComponent] stringByDeletingPathExtension];
    NSString *storeExtension = [[sourceStoreURL path] pathExtension];
    NSString *storePath = [[sourceStoreURL path] stringByDeletingPathExtension];
    //Build a path to write the new store
    storePath = [NSString stringWithFormat:@"%@.%@.%@", storePath, modelName, storeExtension];
    NSURL *destinationStoreURL = [NSURL fileURLWithPath:storePath];
    
    if (![manager migrateStoreFromURL:sourceStoreURL type:type options:nil withMappingModel:mappingModel toDestinationURL:destinationStoreURL destinationType:type destinationOptions:nil error:error]) {
        return NO;
    }
    // Migration was successful, move the files around to preserve the source
    NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString];
    guid = [guid stringByAppendingPathExtension:modelName];
    guid = [guid stringByAppendingPathExtension:storeExtension];
    NSString *appSupportPath = [storePath stringByDeletingLastPathComponent];
    NSString *backupPath = [appSupportPath stringByAppendingPathComponent:guid];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager moveItemAtPath:[sourceStoreURL path] toPath:backupPath error:error]) {
        // Failed to copy the file
        return NO;
    }
    // Move the destination to the source path
    if (![fileManager moveItemAtPath:storePath toPath:[sourceStoreURL path] error:error]) {
        //Try to back out the source move first, no point in checking it for errors
        [fileManager moveItemAtPath:backupPath toPath:[sourceStoreURL path] error:nil];
        return NO;
    }
    // We may not be at the "current" model yet, so recurse
    return [self progressivelyMigrateURL:sourceStoreURL ofType:type toModel:finalModel error:error];
}

- (NSPersistentStoreCoordinator*)persistentStoreCoordinator {
    if (__persistentStoreCoordinator) return __persistentStoreCoordinator;

    NSURL *storeURL = [[AppDelegate applicationDocumentsDirectory] URLByAppendingPathComponent:@"Archery_Tools.sqlite"];
    NSManagedObjectModel *mom = [self managedObjectModel];
    __persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:mom];
    
    NSError *error = nil;
    if ([self progressivelyMigrateURL:storeURL ofType:NSSQLiteStoreType toModel:mom error:&error]) {
        if ([__persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
            return __persistentStoreCoordinator;
        }
    }
    
    NSLog(@"No CoreData store. No migration required.");
    return nil;
}

- (NSManagedObjectContext *)managedObjectContext {
    if (__managedObjectContext != nil) {
        return __managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        __managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
        [__managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return __managedObjectContext;
}

#pragma mark end - Leagcy Core Data stack

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
+ (NSURL *)applicationDocumentsDirectory {
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

#pragma mark end - Application's Documents directory

@end
