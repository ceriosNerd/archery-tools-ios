//
//  EndInfoViewController.m
//  Archery Tools
//
//  Created by Andrew Querol on 4/3/12.
//  Copyright (c) 2012 Cerios Game Studios. All rights reserved.
//

#import "EndInfoViewController.h"
#import "AppDelegate.h"

@implementation EndInfoViewController

@synthesize targetPictureView;
@synthesize imgPicker;
@synthesize imageURL;

- (void)viewDidLoad
{
    [super viewDidLoad];
    imgPicker = [[UIImagePickerController alloc] init];
//	if (end.image) {
//        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
//        dispatch_async(queue, ^{ 
//            UIImage *image = [UIImage imageWithData:end.image];
//            [self performSelectorOnMainThread:@selector(imageLoadDone:) withObject:image waitUntilDone:NO];
//        });
//    }
}

- (void)viewDidUnload
{
    [self setTargetPictureView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (IBAction)cameraButton:(id)sender{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] == YES) {
        
        // Set source to the camera
        imgPicker.sourceType =  UIImagePickerControllerSourceTypeCamera;
    
        // Delegate is self
        imgPicker.delegate = self;
    
        //Allow editing of image ?
        [imgPicker setAllowsEditing:FALSE];
    
        // Show image picker
        [self presentViewController:imgPicker animated:true completion:nil];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"No camera is avaiable!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
}

- (IBAction)clearPhotoButton:(id)sender {
//    if (end.image) {
//        [end setImage:nil];
//    }
//    targetPictureView.image = nil;
//    [end.managedObjectContext save:nil];
}

- (IBAction)backButton:(id)sender {
    [self.navigationController popViewControllerAnimated:TRUE];
}

- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {

    UIImage *image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{ 
        NSData *data = UIImagePNGRepresentation([self imageWithImage:image scaledToSize:targetPictureView.frame.size]);
        [self performSelectorOnMainThread:@selector(imageProssessingDone:) withObject:data waitUntilDone:NO];
    });
    
    [targetPictureView setImage:image];
    [picker dismissViewControllerAnimated:true completion:nil];
}

- (void) imageProssessingDone: (NSData *) image {
//    [end setImage:image];
//    [end.managedObjectContext save:nil];
}

- (void) imageLoadDone: (UIImage *) image {
    [targetPictureView setImage:image];
}

- (UIImage*)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize;
{
    UIGraphicsBeginImageContext( newSize );
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

@end
