//
//  EventTypesVersion1ToVersion2.h
//  Archery Tools
//
//  Created by Andrew Querol on 4/19/12.
//  Copyright (c) 2012 Cerios Game Studios. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface EventTypesVersion1ToVersion2 : NSEntityMigrationPolicy

@end
