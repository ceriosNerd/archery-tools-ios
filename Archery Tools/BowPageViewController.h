//
//  BowPageViewController.h
//  Archery Tools
//
//  Created by Andrew Querol on 5/17/14.
//  Copyright (c) 2014 Cerios Game Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol BowPageCompliant <NSObject>
@required
@property NSNumber *bowId;
@end

@interface BowPageViewController : UIViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate>

- (IBAction)cameraButton:(id)sender;
- (IBAction)onClearPress:(id)sender;

@property (strong) NSNumber *bowId;
@property UIImagePickerController *imagePicker;
@property (weak, nonatomic) IBOutlet UILabel *bowNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *bowPictureView;
@property (weak, nonatomic) IBOutlet UIView *clearButtonFrame;
@end
