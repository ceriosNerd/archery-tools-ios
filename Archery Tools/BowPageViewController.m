//
//  BowPageViewController.m
//  Archery Tools
//
//  Created by Andrew Querol on 5/17/14.
//  Copyright (c) 2014 Cerios Game Studios. All rights reserved.
//

#import "BowPageViewController.h"
#import "SightMarksViewController.h"
#import "NotesViewController.h"
#import "MeasurementsViewController.h"
#import "SQLiteSingleton.h"
#import "AppDelegate.h"

@implementation BowPageViewController

@synthesize bowId;
@synthesize bowNameLabel;
@synthesize bowPictureView;
@synthesize imagePicker;
@synthesize clearButtonFrame;

- (void)viewDidLoad {
    [super viewDidLoad];
    NSString *bowImage = [self retreiveBowImage];
    imagePicker = [[UIImagePickerController alloc] init];
	if (bowImage && bowImage.length > 0) {
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        dispatch_async(queue, ^{
            UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:bowImage relativeToURL:[AppDelegate applicationDocumentsDirectory]]]];
            [self performSelectorOnMainThread:@selector(imageLoadDone:) withObject:image waitUntilDone:NO];
        });
    }
}

- (NSString *) retreiveBowImage {
    sqlite3_stmt *bowStatement;
    NSString *bowQuery = [NSString stringWithFormat:@"SELECT name, image_path FROM bows WHERE `id`='%lli'", [bowId longLongValue]];
    sqlite3_prepare_v2([[SQLiteSingleton getInstance] getDatabase], [bowQuery UTF8String], -1, &bowStatement, nil);
    
    if (sqlite3_step(bowStatement) != SQLITE_ROW) {
        NSLog(@"Failed to retreive the bow name, image for bow ID: %lli", [bowId longLongValue]);
        return nil;
    }
    [bowNameLabel setText:[NSString stringWithUTF8String:(const char*) sqlite3_column_text(bowStatement, 0)]];
    NSString *bowImage = nil;
    if (sqlite3_column_text(bowStatement, 1) != nil)
        bowImage = [NSString stringWithUTF8String:(const char*) sqlite3_column_text(bowStatement, 1)];
    sqlite3_finalize(bowStatement);
    return bowImage;
}

- (BOOL)shouldAutorotate {
    return YES;
}

- (void)viewDidUnload {
    [bowPictureView setImage:nil];
    [super viewDidUnload];
}

- (IBAction)cameraButton:(id)sender{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] == YES) {
        
        // Set source to the camera
        imagePicker.sourceType =  UIImagePickerControllerSourceTypeCamera;
        
        // Delegate is self
        imagePicker.delegate = self;
        
        //Allow editing of image ?
        [imagePicker setAllowsEditing:true];
        
        // Show image picker
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(300 * NSEC_PER_MSEC)), dispatch_get_main_queue(), ^{
            [self.parentViewController presentViewController:imagePicker animated:true completion:nil];
        });
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"No camera is avaiable!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
}

- (IBAction)onClearPress:(id)sender {
    NSString *bowImage = [self retreiveBowImage];
    if (bowImage && bowImage.length > 0) {
        NSURL *bowImageURL = [NSURL URLWithString:bowImage relativeToURL:[AppDelegate applicationDocumentsDirectory]];
        NSError *error;
        [[NSFileManager defaultManager] removeItemAtURL:bowImageURL error:&error];
        if (error)
            NSLog(@"Failed to remove photo file: %@", bowImage);
    }
    sqlite3_exec([[SQLiteSingleton getInstance] getDatabase], [[NSString stringWithFormat:@"UPDATE bows SET `image_path`='', `synced`='0' WHERE `id`=%lli", [bowId longLongValue]] UTF8String], nil, nil, nil);
    [clearButtonFrame setHidden:true];
}

- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        NSData *data = UIImagePNGRepresentation([self imageWithImage:image scaledToSize:bowPictureView.frame.size]);
        [self performSelectorOnMainThread:@selector(imageProssessingDone:) withObject:data waitUntilDone:NO];
    });
    [picker dismissViewControllerAnimated:true completion:nil];
}

- (void) imageProssessingDone: (NSData *)image {
    NSURL *imageUrl = [NSURL URLWithString:[NSString stringWithFormat:@"images/%@-Picture@%lu.png", bowNameLabel.text, (unsigned long)time(NULL)] relativeToURL:[AppDelegate applicationDocumentsDirectory]];
    [image writeToURL:imageUrl atomically:true];
    sqlite3_exec([[SQLiteSingleton getInstance] getDatabase], [[NSString stringWithFormat:@"UPDATE bows SET `image_path`='%@', `synced`='0' WHERE `id`=%lli", [imageUrl relativeString], [bowId longLongValue]] UTF8String], nil, nil, nil);
    [self performSelectorOnMainThread:@selector(imageLoadDone:) withObject:image waitUntilDone:NO];
}

- (void) imageLoadDone: (UIImage *) image {
    [bowPictureView setImage:image];
    [clearButtonFrame setHidden:false];
    [bowPictureView setNeedsLayout];
    [bowPictureView layoutIfNeeded];
}

- (UIImage*)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize;
{
    UIGraphicsBeginImageContext( newSize );
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.destinationViewController conformsToProtocol:@protocol(BowPageCompliant)]) {
        id<BowPageCompliant> dest = segue.destinationViewController;
        [dest setBowId:bowId];
    }
}

@end
