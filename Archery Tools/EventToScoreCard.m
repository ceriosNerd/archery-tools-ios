//
//  EventToScoreCard.m
//  Archery Tools
//
//  Created by Andrew Querol on 5/18/14.
//  Copyright (c) 2014 Cerios Game Studios. All rights reserved.
//

#import "EventToScoreCard.h"
#import "NSManagedObjectModel+KCOrderedAccessorFix.h"

@implementation EventToScoreCard

- (BOOL)createDestinationInstancesForSourceInstance:(NSManagedObject *)sInstance entityMapping:(NSEntityMapping *)mapping manager:(NSMigrationManager *)manager error:(NSError *__autoreleasing *)error {
    
    // Set up descriptions
    NSEntityDescription *eventDisc = [NSEntityDescription entityForName:@"Event" inManagedObjectContext:[manager destinationContext]];
    [manager.destinationModel kc_generateOrderedSetAccessorsForEntity:eventDisc];
    NSEntityDescription *roundDisc = [NSEntityDescription entityForName:@"Round" inManagedObjectContext:[manager destinationContext]];
    [manager.destinationModel kc_generateOrderedSetAccessorsForEntity:roundDisc];
    NSEntityDescription *endDisc = [NSEntityDescription entityForName:@"End" inManagedObjectContext:[manager destinationContext]];
    [manager.destinationModel kc_generateOrderedSetAccessorsForEntity:endDisc];
    NSEntityDescription *arrowDisc = [NSEntityDescription entityForName:@"Arrow" inManagedObjectContext:[manager destinationContext]];
    [manager.destinationModel kc_generateOrderedSetAccessorsForEntity:arrowDisc];
    
    NSManagedObject *scoreCardObject = [NSEntityDescription insertNewObjectForEntityForName:[mapping destinationEntityName] inManagedObjectContext:[manager destinationContext]];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterLongStyle];
    [scoreCardObject setValue:[dateFormatter dateFromString:[sInstance valueForKey:@"date"]] forKey:@"eventDate"];
    [scoreCardObject setValue:[sInstance valueForKey:@"name"] forKey:@"eventName"];
    [scoreCardObject setValue:[[[sInstance mutableSetValueForKey:@"archer"] anyObject] valueForKey:@"name"] forKey:@"archersName"];
    NSManagedObject *newEvent = [NSEntityDescription insertNewObjectForEntityForName:@"Event" inManagedObjectContext:[manager destinationContext]];
    NSString *keyboardUrl = nil;
    NSMutableSet *rounds = [[[sInstance mutableSetValueForKey:@"archer"] anyObject] mutableSetValueForKey:@"rounds"];
    NSManagedObject *eventTypeRound = [[sInstance mutableSetValueForKeyPath:@"eventType.rounds"] anyObject];
    NSNumber *keyboardNumber = [eventTypeRound valueForKey:@"keyboardType"];
    
    switch (keyboardNumber.intValue) {
        case 0:
            keyboardUrl = @"fita_keyboard.xml";
            break;
        case 1:
            keyboardUrl = @"nfaa_keyboard.xml";
            break;
        case 2:
            keyboardUrl = @"nfaa_animal_keyboard.xml";
            break;
        case 3:
            keyboardUrl = @"ibo_keyboard.xml";
            break;
        case 4:
            keyboardUrl = @"asa_keyboard.xml";
            break;
        case 5:
            keyboardUrl = @"fita_field_keyboard.xml";
            break;
        case 6:
            keyboardUrl = @"five_zone_keyboard.xml";
            break;
        default:
            keyboardUrl = @"fita_keyboard.xml";
            break;
    }
    [newEvent setValue:keyboardUrl forKey:@"keyboardFile"];
    [newEvent setValue:[sInstance valueForKeyPath:@"eventType.name"] forKey:@"typeName"];
    [newEvent setValue:scoreCardObject forKey:@"parent"];
    [scoreCardObject setValue:newEvent forKey:@"event"];
    
    for (NSManagedObject *round in rounds) {
        NSManagedObject *newRound = [NSEntityDescription insertNewObjectForEntityForName:@"Round" inManagedObjectContext:[manager destinationContext]];
        [newRound setValue:[eventTypeRound valueForKey:@"arrowsPerEnd"] forKey:@"arrowsPerEnd"];
        [newRound setValue:[eventTypeRound valueForKey:@"endsPerRound"] forKey:@"endsPerRound"];
        [newRound setValue:[round valueForKey:@"roundNumber"] forKey:@"roundNumber"];
        [newRound setValue:[round valueForKey:@"hits"] forKey:@"hits"];
        [newRound setValue:[round valueForKey:@"avgScore"] forKey:@"avgScore"];
        [newRound setValue:[round valueForKey:@"total"] forKey:@"total"];
        [newRound setValue:[round valueForKey:@"totalArrows"] forKey:@"totalShot"];
        [newRound setValue:newEvent forKey:@"parent"];
        
        for (NSManagedObject *end in [round mutableSetValueForKey:@"ends"]) {
            NSManagedObject *newEnd = [NSEntityDescription insertNewObjectForEntityForName:@"End" inManagedObjectContext:[manager destinationContext]];
            [newEnd setValue:[end valueForKey:@"endNumber"] forKey:@"endNumber"];
            [newEnd setValue:[end valueForKey:@"endTotal"] forKey:@"endTotal"];
            [newEnd setValue:[end valueForKey:@"hits"] forKey:@"hits"];
            [newEnd setValue:[end valueForKey:@"runningTotal"] forKey:@"runningTotal"];
            [newEnd setValue:[end valueForKeyPath:@"image.image"] forKey:@"image"];
            [newEnd setValue:[NSNumber numberWithUnsignedInteger:[[end mutableSetValueForKey:@"arrows"] count]] forKey:@"shots"];
            [newEnd setValue:newRound forKey:@"parent"];
            
            for (int i = 0; i < [[eventTypeRound valueForKey:@"arrowsPerEnd"] integerValue]; i++) {
                NSManagedObject *newArrow = [NSEntityDescription insertNewObjectForEntityForName:@"Arrow" inManagedObjectContext:[manager destinationContext]];
                NSManagedObject *arrow = [[[end mutableSetValueForKey:@"arrows"] objectsPassingTest:^BOOL(id obj, BOOL *stop) {
                    if ([[obj valueForKey:@"arrowNumber"] integerValue] == i) {
                        return true;
                    }
                    return false;
                }] anyObject];
                if (arrow == nil) {
                    [newArrow setValue:[NSNumber numberWithInt:i] forKey:@"arrowNumber"];
                } else {
                    [newArrow setValue:[arrow valueForKey:@"arrowNumber"] forKey:@"arrowNumber"];
                    [newArrow setValue:[arrow valueForKey:@"arrowText"] forKey:@"arrowText"];
                    [newArrow setValue:[arrow valueForKey:@"arrowValue"] forKey:@"arrowValue"];
                    [newArrow setValue:[self getColorForArrowValue:[[arrow valueForKey:@"arrowValue"] integerValue] keyboardType:keyboardNumber.intValue] forKey:@"arrowImage"];
                    [newArrow setValue:[self getTextColorForArrowValue:[[arrow valueForKey:@"arrowValue"] integerValue] keyboardType:keyboardNumber.intValue] forKey:@"textColor"];
                }
                [newArrow setValue:newEnd forKey:@"parent"];
                [[newEnd mutableOrderedSetValueForKey:@"arrows"] addObject:newArrow];
            }
            [[newRound mutableOrderedSetValueForKey:@"ends"] addObject:newEnd];
        }
        [[newEvent mutableOrderedSetValueForKey:@"rounds"] addObject:newRound];
    }
    return [[manager destinationContext] save:error];
}

- (NSString *) getColorForArrowValue:(NSInteger)value keyboardType:(NSInteger)type {
    switch (type) {
        case 0:
            switch (value) {
                case 10:
                case 9:
                    return @"yellow";
                    break;
                case 8:
                case 7:
                    return @"red";
                    break;
                case 6:
                case 5:
                    return @"blue";
                    break;
                case 4:
                case 3:
                    return @"black";
                    break;
                case 2:
                case 1:
                case 0:
                    return @"white";
                    break;
                case -1:
                default:
                    return @"gray";
                    break;
            }
        case 1:
            switch (value) {
                case 5:
                case 2:
                case 1:
                case 0:
                    return @"white";
                    break;
                case 4:
                case 3:
                    return @"blue";
                    break;
                case -1:
                default:
                    return @"gray";
                    break;
            }
            break;
        case 2:
            switch (value) {
                case 20:
                case 18:
                    return @"red";
                    break;
                case 16:
                case 14:
                    return @"yellow";
                    break;
                case 12:
                case 10:
                    return @"blue";
                    break;
                case 0:
                    return @"white";
                    break;
                case -1:
                default:
                    return @"gray";
                    break;
            }
            break;
        case 3:
            switch (value) {
                case 10:
                case 11:
                    return @"red";
                    break;
                case 8:
                    return @"yellow";
                    break;
                case 5:
                    return @"blue";
                    break;
                case 0:
                    return @"white";
                    break;
                case -1:
                default:
                    return @"gray";
                    break;
            }
            break;
        case 4:
            switch (value) {
                case 14:
                case 12:
                    return @"red";
                    break;
                case 10:
                case 8:
                    return @"yellow";
                    break;
                case 5:
                    return @"blue";
                    break;
                case 0:
                    return @"white";
                    break;
                case -1:
                default:
                    return @"gray";
                    break;
            }
            break;
        case 5:
            switch (value) {
                case 6:
                case 5:
                    return @"yellow";
                    break;
                case 4:
                case 3:
                case 2:
                case 1:
                    return @"black";
                    break;
                case 0:
                    return @"white";
                    break;
                case -1:
                default:
                    return @"gray";
                    break;
            }
            break;
        case 6:
            switch (value) {
                case 9:
                    return @"yellow";
                    break;
                case 7:
                    return @"red";
                    break;
                case 5:
                    return @"blue";
                    break;
                case 3:
                    return @"black";
                    break;
                case 1:
                case 0:
                    return @"white";
                    break;
                case -1:
                default:
                    return @"gray";
                    break;
            }
            break;
        default:
            return @"gray";
            break;
    }
}

- (NSString *) getTextColorForArrowValue:(NSInteger)value keyboardType:(NSInteger)type {
    switch (type) {
        case 0:
            switch (value) {
                case 6:
                case 5:
                case 4:
                case 3:
                    return @"whiteColor";
                    break;
                default:
                    return @"blackColor";
                    break;
            }
            break;
        case 1:
            switch (value) {
                case 4:
                case 3:
                    return @"whiteColor";
                    break;
                default:
                    return @"blackColor";
                    break;
            }
            break;
        case 2:
            switch (value) {
                case 12:
                case 10:
                    return @"whiteColor";
                    break;
                default:
                    return @"blackColor";
                    break;
            }
            break;
        case 3:
            switch (value) {
                case 5:
                    return @"whiteColor";
                    break;
                default:
                    return @"blackColor";
                    break;
            }
            break;
        case 4:
            switch (value) {
                case 5:
                    return @"whiteColor";
                    break;
                default:
                    return @"blackColor";
                    break;
            }
            break;
        case 5:
            switch (value) {
                case 4:
                case 3:
                case 2:
                case 1:
                    return @"whiteColor";
                    break;
                default:
                    return @"blackColor";
                    break;
            }
            break;
        case 6:
            switch (value) {
                case 5:
                case 3:
                    return @"whiteColor";
                    break;
                default:
                    return @"blackColor";
                    break;
            }
            break;
        default:
            return @"blackColor";
            break;
    }
}

@end
