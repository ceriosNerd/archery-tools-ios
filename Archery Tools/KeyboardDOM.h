//
//  KeyboardDOM.h
//  Archery Tools
//
//  Created by Andrew Querol on 5/16/14.
//  Copyright (c) 2014 Cerios Game Studios. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol KeyboardDelegate <NSObject>

- (void) keyPress: (NSString*)text integerValue:(NSInteger)value sortOrder:(NSInteger)rank backgroundImage:(NSString *)backgroundImage textColor:(NSString *)textColor;
- (void) infoPress;
- (void) deletePress;
- (void) closeKeyboard;

@end

@interface KeyboardDOM : NSObject

@property NSString *name;
@property NSString *fileName;
@property NSMutableArray *rows;
@property NSMutableArray *specialKeys;

@end
