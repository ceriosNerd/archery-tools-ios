//
//  ScoreEndCell.m
//  Archery Tools
//
//  Created by Andrew Querol on 3/28/12.
//  Copyright (c) 2012 Cerios Game Studios. All rights reserved.
//

#import "ScoreEndCell.h"
#import "ArcheryTextLabel.h"
#import "SQLiteSingleton.h"

@implementation ScoreEndCell {
    int endId;
}

@synthesize selectedTag;
@synthesize delgate;
@synthesize archeryTextFields;
@synthesize tableView;
@synthesize numberOfArrows;
@synthesize keyboard;

@synthesize archeryTextLabels;
@synthesize endNumberTextLabel;
@synthesize endTotalTextLabel;
@synthesize hitsNumberTextLabel;
@synthesize runningTotalTextLabel;

- (id)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        archeryTextFields = [NSMutableOrderedSet orderedSet];
    }
    return self;
}

- (void)setEndId:(int)end {
    endId = end;
}

- (int)getEndId {
    return endId;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (ArcheryTextLabel *) labelFromTag: (NSInteger) tag {
    if ([self.archeryTextFields count] <= tag) {
        return nil;
    }
    return [self.archeryTextFields objectAtIndex: tag];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    if ([[[touches anyObject] view] isMemberOfClass:[ArcheryTextLabel class]]) {
        [self clearSelection];
        selectedTag = [[[touches anyObject] view] tag];
        [[self labelFromTag: selectedTag] setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"green"]]];
        [[self labelFromTag: selectedTag] setText:@"?"];
    }
}

- (void) clearSelection {
    if ([[self labelFromTag: selectedTag].text  isEqual: @"?"]) {
        [ScoreEndCell clearLabel:[self labelFromTag: selectedTag]];
    }
}

+ (void) clearLabel: (ArcheryTextLabel *) label {
    [label setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"gray"]]];
    [label setText:@""];
}

- (void) nextArcheryTextLabel {
    ArcheryTextLabel *next = [self labelFromTag:selectedTag + 1];
    if (next && !next.isHidden) {
        [next becomeFirstResponder];
        [self clearSelection];
        selectedTag++;
        [next setText:@"?"];
        [next setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"green"]]];
    } else {
        [self.delgate updateLabels:[self.tableView indexPathForCell:self]];
        [self clearSelection];
        [self closeKeyboard];
    }
}

- (void) prevArcheryTextLabel {
    ArcheryTextLabel *prev = [self labelFromTag:selectedTag - 1];
    if (prev && !prev.isHidden) {
        [prev becomeFirstResponder];
        [self clearSelection];
        selectedTag--;
        [[self labelFromTag:selectedTag] setText:@"?"];
        [[self labelFromTag:selectedTag] setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"green"]]];
    } else {
        [self.delgate updateLabels:[self.tableView indexPathForCell:self]];
        [self clearSelection];
        [self closeKeyboard];
    }
}

- (void) closeKeyboard {
    [[self labelFromTag: selectedTag] resignFirstResponder];
    [self sortArrows];
    [self setArrowLabels];
}

- (void)prepareForReuse {
    selectedTag = -1;
    [archeryTextFields removeAllObjects];
    for (ArcheryTextLabel *label in archeryTextLabels) {
        [label setHidden:FALSE];
        [label setUserInteractionEnabled:TRUE];
        [ScoreEndCell clearLabel:label];
    }
    
    [endTotalTextLabel setText:@""];
    [endNumberTextLabel setText:@""];
    [hitsNumberTextLabel setText:@""];
    [runningTotalTextLabel setText:@""];
    
    [super prepareForReuse];
}

- (void) setRunningTotal: (NSString *) total {
    [runningTotalTextLabel setText:total];
}

- (void) setEndTotal: (NSString *) endTotal {
    [endTotalTextLabel setText:endTotal];
}

- (void) setHits: (NSString *) hits {
    [hitsNumberTextLabel setText:hits];
}

- (void) initCell {
    for (ArcheryTextLabel *label in archeryTextLabels) {
        if (label.tag > [self numberOfArrows] - 1) { // 1 Based index
            [label setHidden:TRUE];
            [label setUserInteractionEnabled:FALSE];
            continue;
        }
        [label setKeyboardView:keyboard];
        [label setParent:self];
        [[self archeryTextFields] addObject:label];
        [label setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"gray"]]];
    }
    
    [[self archeryTextFields] sortUsingComparator: ^(id obj1, id obj2) {
        ArcheryTextLabel *label1 = (ArcheryTextLabel *) obj1;
        ArcheryTextLabel *label2 = (ArcheryTextLabel *)obj2;
        
        if (label1.tag > label2.tag) {
            return NSOrderedDescending;
        } else if (label1.tag == label2.tag) {
            return NSOrderedSame;
        } else {
            return NSOrderedAscending;
        }
    }];
    
    [self setArrowLabels];
}

- (void) setArrowLabels {
    sqlite3_stmt *arrowStatement;
    NSString *arrowQuery = [NSString stringWithFormat:@"SELECT number, text_value, image, text_color FROM arrows WHERE `end_id`='%d' ORDER BY number ASC", endId];
    sqlite3_prepare_v2([[SQLiteSingleton getInstance] getDatabase], [arrowQuery UTF8String], -1, &arrowStatement, nil);
    
    while (sqlite3_step(arrowStatement) == SQLITE_ROW) {
        ArcheryTextLabel *arrowLabel = [archeryTextFields  objectAtIndex:(sqlite3_column_int(arrowStatement, 0))];
        [arrowLabel setText:[NSString stringWithUTF8String:(const char*)sqlite3_column_text(arrowStatement, 1)]];
        
        NSString *arrowImage = [NSString stringWithUTF8String:(const char*)sqlite3_column_text(arrowStatement, 2)];
        NSString *arrowTextColor = [NSString stringWithUTF8String:(const char*)sqlite3_column_text(arrowStatement, 3)];
        
        if (arrowImage != nil && [arrowImage length] > 0)
            [arrowLabel setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:arrowImage]]];
        else
            [arrowLabel setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"gray"]]];
        if (arrowTextColor != nil && [arrowTextColor length] > 0)
            [arrowLabel setTextColor:[UIColor performSelector:NSSelectorFromString(arrowTextColor)]];
    }
    sqlite3_finalize(arrowStatement);
}

- (void) sortArrows {
    sqlite3_stmt *sortStatement;
    NSString *sortQuery = [NSString stringWithFormat:@"SELECT id FROM arrows WHERE `end_id`='%d' ORDER BY (CASE WHEN sort_order < 0 THEN 9223372036854775807 ELSE sort_order END) ASC", endId];
    sqlite3_prepare_v2([[SQLiteSingleton getInstance] getDatabase], [sortQuery UTF8String], -1, &sortStatement, nil);
    
    NSMutableArray *idSet = [[NSMutableArray alloc] init];
    while (sqlite3_step(sortStatement) == SQLITE_ROW) {
        [idSet addObject:[NSNumber numberWithInt:sqlite3_column_int(sortStatement, 0)]];
    }
    sqlite3_finalize(sortStatement);
    
    NSMutableString *updateQuery = [NSMutableString stringWithString:@"UPDATE arrows SET `number`=CASE `id`, `synced`='0' "];
    NSMutableString *whereClause = [NSMutableString stringWithString:@"WHERE `id` IN ("];
    int number = 0;
    for (NSNumber *arrowId in idSet) {
        [updateQuery appendFormat:@"WHEN '%i' THEN '%i' ", [arrowId intValue], number];
        if (number == 0)
            [whereClause appendFormat:@"%i", [arrowId intValue]];
        else
            [whereClause appendFormat:@",%i", [arrowId intValue]];
        number++;
    }
    [whereClause appendString:@");"];
    [updateQuery appendFormat:@"END %@", whereClause];
    sqlite3_exec([[SQLiteSingleton getInstance] getDatabase], [updateQuery UTF8String], nil, nil, nil);
}

- (void) update {
    sqlite3_stmt *endTotalStatement;
    NSString *endTotalQuery = [NSString stringWithFormat:@"SELECT SUM(CASE WHEN `value` < 0 THEN 0 ELSE `value` END) AS total, COUNT(value) AS shots, COUNT(CASE WHEN `value` > 0 THEN `value` ELSE NULL END) AS hits FROM arrows WHERE `end_id`='%i'", endId];
    sqlite3_prepare_v2([[SQLiteSingleton getInstance] getDatabase], [endTotalQuery UTF8String], -1, &endTotalStatement, nil);
    if (sqlite3_step(endTotalStatement) != SQLITE_ROW) {
        NSLog(@"Failed to sum the end total for end ID:%i", endId);
        sqlite3_finalize(endTotalStatement);
        return;
    }
    int total = sqlite3_column_int(endTotalStatement, 0);
    int shots = sqlite3_column_int(endTotalStatement, 1);
    int hits = sqlite3_column_int(endTotalStatement, 2);
    sqlite3_finalize(endTotalStatement);
    
    NSMutableString *updateQuery = [NSMutableString stringWithFormat:@"UPDATE ends SET `total`='%i', `shots`='%i', `hits`='%i', `synced`='0' WHERE `id`='%i'", total, shots, hits, endId];
    sqlite3_exec([[SQLiteSingleton getInstance] getDatabase], [updateQuery UTF8String], nil, nil, nil);
    
    sqlite3_stmt *runningTotalStatement;
    NSString *runningTotalQuery = [NSString stringWithFormat:@"SELECT total, hits, shots FROM ends WHERE `round_id`=(SELECT round_id FROM ends WHERE `id`='%i') ORDER BY number ASC", endId];
    sqlite3_prepare_v2([[SQLiteSingleton getInstance] getDatabase], [runningTotalQuery UTF8String], -1, &runningTotalStatement, nil);
    
    int runningTotal = 0, totalHits = 0, totalShots = 0;
    NSMutableArray *runningTotals = [[NSMutableArray alloc] init];
    while (sqlite3_step(runningTotalStatement) == SQLITE_ROW) {
        runningTotal += sqlite3_column_int(runningTotalStatement, 0);
        totalHits += sqlite3_column_int(runningTotalStatement, 1);
        totalShots += sqlite3_column_int(runningTotalStatement, 2);
        [runningTotals addObject:[NSNumber numberWithInt:runningTotal]];
    }
    sqlite3_finalize(runningTotalStatement);
    
    updateQuery = [NSMutableString stringWithString:@"UPDATE ends SET `synced`='0', `running_total`=(CASE `number` "];
    int number = 1;
    for (NSNumber *runningTotal in runningTotals) {
        [updateQuery appendFormat:@"WHEN '%i' THEN '%i' ", number, [runningTotal intValue]];
        number++;
    }
    [updateQuery appendFormat:@"END) WHERE `round_id`=(SELECT round_id FROM ends WHERE `id`='%i');", endId];
    sqlite3_exec([[SQLiteSingleton getInstance] getDatabase], [updateQuery UTF8String], nil, nil, nil);
    
    updateQuery = [NSMutableString stringWithFormat:@"UPDATE rounds SET `total`='%i', `hits`='%i', `total_shot`='%i', `average_score`='%f', `synced`='0' WHERE `id`=(SELECT round_id FROM ends WHERE `id`='%i')", runningTotal, totalHits, totalShots, (double)runningTotal/(double)totalShots, endId];
    sqlite3_exec([[SQLiteSingleton getInstance] getDatabase], [updateQuery UTF8String], nil, nil, nil);
    
    [delgate updateLabels:[self.tableView indexPathForCell:self]];
    [self refreshText];
}

- (void) refreshText {
    sqlite3_stmt *endStatement;
    NSString *endQuery = [NSString stringWithFormat:@"SELECT total, hits, running_total FROM ends WHERE `id`='%d'", endId];
    sqlite3_prepare_v2([[SQLiteSingleton getInstance] getDatabase], [endQuery UTF8String], -1, &endStatement, nil);
    if (sqlite3_step(endStatement) != SQLITE_ROW) {
        NSLog(@"Failed to refresh cell for end ID:%i", endId);
        sqlite3_finalize(endStatement);
        return;
    }
    [endTotalTextLabel setText: [NSString stringWithFormat:@"%i", sqlite3_column_int(endStatement, 0)]];
    [hitsNumberTextLabel setText:[NSString stringWithFormat:@"%i", sqlite3_column_int(endStatement, 1)]];
    [runningTotalTextLabel setText:[NSString stringWithFormat:@"%i", sqlite3_column_int(endStatement, 2)]];
    sqlite3_finalize(endStatement);
}

//===========================================================
// - (void) keyPress: (NSString*)text integerValue:(NSInteger)value
//
//===========================================================
- (void) keyPress: (NSString*)text integerValue:(NSInteger)value sortOrder:(NSInteger)rank backgroundImage:(NSString *)backgroundImage textColor:(NSString *)textColor {
    NSString *updateArrowQuery = [NSString stringWithFormat:@"UPDATE arrows SET `value`='%li', `text_value`='%@', `image`='%@', `text_color`='%@', `sort_order`='%li', `synced`='0' WHERE number=%li AND end_id=%i", (long)value, text, backgroundImage, textColor, (long)rank, (long)selectedTag, endId];
    sqlite3_exec([[SQLiteSingleton getInstance] getDatabase], [updateArrowQuery UTF8String], nil, nil, nil);
    
    ArcheryTextLabel *label = [self labelFromTag:selectedTag];
    [label setText:text];
    [label setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:backgroundImage]]];
    [label setTextColor:[UIColor performSelector:NSSelectorFromString(textColor)]];
    
    [self nextArcheryTextLabel];
    [self update];
} //keyPress:integerValue:

//===========================================================
// - (void) infoPress
//
//===========================================================
- (void) infoPress {
    [self.delgate infoPush:endId];
    [self clearSelection];
} //infoPress

//===========================================================
// - (void) deletePress
//
//===========================================================
- (void) deletePress {
    NSString *updateArrowQuery = [NSString stringWithFormat:@"UPDATE arrows SET `value`='%i', `text_value`='%@', `image`='%@', `text_color`='%@', `synced`='0' WHERE number=%li AND end_id=%i", 0, @"", @"gray", @"blackColor", (long)selectedTag, endId];
    sqlite3_exec([[SQLiteSingleton getInstance] getDatabase], [updateArrowQuery UTF8String], nil, nil, nil);
    
    ArcheryTextLabel *label = [self labelFromTag:selectedTag];
    [label setText:@""];
    [label setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"gray"]]];
    [label setTextColor:[UIColor blackColor]];
    
    [self prevArcheryTextLabel];
    [self update];
} //deletePress


@end
