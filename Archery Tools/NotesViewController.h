//
//  NotesViewController.h
//  Archery Tools
//
//  Created by Andrew Querol on 3/25/12.
//  Copyright (c) 2012 Cerios Game Studios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BowPageViewController.h"

@interface NotesViewController : UITableViewController <BowPageCompliant>

@end
