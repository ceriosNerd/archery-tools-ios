//
//  BowViewController.h
//  Archery Tools
//
//  Created by Andrew Querol on 5/17/14.
//  Copyright (c) 2014 Cerios Game Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BowViewController : UITableViewController <UIAlertViewDelegate>

- (IBAction)onClick:(id)sender;

@end
