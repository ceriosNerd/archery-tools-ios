//
//  NotesViewController.m
//  Archery Tools
//
//  Created by Andrew Querol on 3/25/12.
//  Copyright (c) 2012 Cerios Game Studios. All rights reserved.
//

#import "NotesViewController.h"
#import "NoteEditCreateViewController.h"
#import "SQLiteSingleton.h"

@implementation NotesViewController

@synthesize bowId;

- (void)viewWillAppear:(BOOL)animated {
    if (bowId != nil)
        [self.tableView reloadData];
    [super viewWillAppear:animated];
}

- (NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section {
    sqlite3_stmt *notesStatement;
    sqlite3_prepare_v2([[SQLiteSingleton getInstance] getDatabase], [[NSString stringWithFormat:@"SELECT COUNT(*) FROM notes WHERE `bow_id`='%lli' ORDER BY id ASC", [bowId longLongValue]] UTF8String], -1, &notesStatement, nil);
    
    int count = 0;
    if (sqlite3_step(notesStatement) == SQLITE_ROW)
        count = sqlite3_column_int(notesStatement, 0);
    sqlite3_finalize(notesStatement);
    
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"note"];
    if (!cell)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"note"];
    
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return true;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        sqlite3_exec([[SQLiteSingleton getInstance] getDatabase], [[NSString stringWithFormat:@"DELETE FROM notes WHERE `id`=(SELECT id FROM notes WHERE `bow_id`='%lli' ORDER BY id ASC LIMIT 1 OFFSET %ld)", [bowId longLongValue], (long)[indexPath row]] UTF8String], nil, nil, nil);
    }
    [self.tableView reloadData];
    
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    sqlite3_stmt *noteStatement;
    NSString *noteQuery = [NSString stringWithFormat:@"SELECT title FROM notes WHERE `bow_id`='%lli' ORDER BY id ASC LIMIT 1 OFFSET %li", [bowId longLongValue], (long) indexPath.row];
    sqlite3_prepare_v2([[SQLiteSingleton getInstance] getDatabase], [noteQuery UTF8String], -1, &noteStatement, nil);
    
    if (sqlite3_step(noteStatement) != SQLITE_ROW) {
        NSLog(@"Failed to retreive the note at: %li", (long) indexPath.row);
        return;
    }
    cell.textLabel.text = [NSString stringWithUTF8String:(const char *) sqlite3_column_text(noteStatement, 0)];
    sqlite3_finalize(noteStatement);
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NoteEditCreateViewController *controller = (NoteEditCreateViewController *) segue.destinationViewController;
    if ([segue.destinationViewController class] == [NoteEditCreateViewController class] && [self.tableView indexPathForSelectedRow]) {
        sqlite3_stmt *noteIdStatement;
        NSString *noteIdQuery = [NSString stringWithFormat:@"SELECT id FROM notes WHERE `bow_id`='%lli' ORDER BY id ASC LIMIT 1 OFFSET %ld", [bowId longLongValue], (long)[self.tableView indexPathForSelectedRow].row];
        sqlite3_prepare_v2([[SQLiteSingleton getInstance] getDatabase], [noteIdQuery UTF8String], -1, &noteIdStatement, nil);
        if (sqlite3_step(noteIdStatement) != SQLITE_ROW) {
            NSLog(@"Failed to retreive the note ID!");
            return;
        }
        long long noteId = sqlite3_column_int64(noteIdStatement, 0);
        sqlite3_finalize(noteIdStatement);
        
        [controller setNoteId:[NSNumber numberWithLongLong:noteId]];
    }
    [controller setBowId:bowId];
    [self setEditing:FALSE];
}

- (void)addClicked {
    [self performSegueWithIdentifier:@"pushNoteEditCreate" sender:self];
}

@end
