//
//  BallisticTrajectory.m
//  Archery Tools
//
//  Created by Andrew Querol on 3/31/12.
//  Copyright (c) 2012 Cerios Game Studios. All rights reserved.
//

#import "BallisticTrajectory.h"

@implementation BallisticTrajectory

- (NSNumber *) getDegrees: (NSNumber *) startX startY:(NSNumber *) startY endX:(NSNumber *)endX endY:(NSNumber *) endY initialVelocity:(NSNumber *) velocity {
    long x = endX.integerValue - startX.integerValue;
    long y = endY.integerValue - startY.integerValue;
    float gravity = 9.81; // m/s
    
    double tmp = pow(velocity.doubleValue, 4) - gravity * (gravity * pow(x, 2) + 2 * y * pow(velocity.doubleValue, 2));
    
    if (tmp < 0) {
        // No solution
        return nil;
    } else {
        double angle1 = atan2(pow(velocity.doubleValue, 2) + sqrt(tmp), gravity * x);
        double angle2 = atan2(pow(velocity.doubleValue, 2) - sqrt(tmp), gravity * x);
        
        if (angle1 < angle2) {
            return [NSNumber numberWithDouble:angle1];
        } else {
            return [NSNumber numberWithDouble:angle2];
        }
    }
}

- (NSNumber *) getInitialVelocity: (NSNumber *) sightToEye distance1:(NSNumber *) distance1 distance2:(NSNumber *) distance2 heightDif: (NSNumber *) height {
    float gravity = 9.81; // m/s
    return [NSNumber numberWithDouble:sqrt(gravity*sightToEye.doubleValue*((distance1.doubleValue*distance2.doubleValue)/height.doubleValue * 2))];
}

@end
