//
//  MeasurementsViewController.h
//  Archery Tools
//
//  Created by Andrew Querol on 5/17/14.
//  Copyright (c) 2014 Cerios Game Studios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BowPageViewController.h"

@interface MeasurementsViewController : UITableViewController <BowPageCompliant, UIAlertViewDelegate>

- (IBAction)addButton:(id)sender;

@end
