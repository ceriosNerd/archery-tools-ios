//
//  RoundDOM.h
//  Archery Tools
//
//  Created by Andrew Querol on 5/15/14.
//  Copyright (c) 2014 Cerios Game Studios. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RoundDOM : NSObject

@property (nonatomic, retain) NSString * distance;
@property (nonatomic, retain) NSNumber * arrowsPerEnd;
@property (nonatomic, retain) NSNumber * endsPerRound;

@end
