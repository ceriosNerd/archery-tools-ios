//
//  RoundDOM.h
//  Archery Tools
//
//  Created by Andrew Querol on 5/12/14.
//  Copyright (c) 2014 Cerios Game Studios. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RoundDOM;

@interface EventDOM : NSObject

@property NSString *name;
@property NSString *keyboardFile;
@property NSMutableArray *rounds;

- (void)addRound:(RoundDOM *)round;

@end
