//
//  CategoryDOM.h
//  Archery Tools
//
//  Created by Andrew Querol on 5/12/14.
//  Copyright (c) 2014 Cerios Game Studios. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CategoryDOM : NSObject

@property NSArray *eventDOMArray;
@property NSString *categoryName;

@end
