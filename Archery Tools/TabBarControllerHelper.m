//
//  TabBarControllerHelper.m
//  Archery Tools
//
//  Created by Andrew Querol on 5/20/14.
//  Copyright (c) 2014 Cerios Game Studios. All rights reserved.
//

#import "TabBarControllerHelper.h"
#import "SightMarkViewController.h"

@implementation TabBarControllerHelper

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self becomeFirstResponder];
}

- (void)motionBegan:(UIEventSubtype)motion withEvent:(UIEvent *)event {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults objectForKey:@"defaultSight"]) {
        SightMarkViewController *sightMarkController = [[self storyboard] instantiateViewControllerWithIdentifier:@"sightMark"];
        NSNumber *sightConfigId = [defaults valueForKey:@"defaultSight"];
        [sightMarkController setSightConfigId:sightConfigId];
        [sightMarkController.view.layer setValue:[NSNumber numberWithBool:true] forKey:@"shook"];
        [((UINavigationController *)self.selectedViewController) pushViewController:sightMarkController animated:TRUE];
    }
}

- (void)updateLayout {
    [UIView animateWithDuration:0.25 animations:^{
        [self.view setNeedsLayout];
        [self.view layoutIfNeeded];
    }];
}

- (BOOL)canBecomeFirstResponder {
    return YES;
}

@end
