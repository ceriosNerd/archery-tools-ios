//
//  ScoreFooterCell.m
//  Archery Tools
//
//  Created by Andrew Querol on 3/28/12.
//  Copyright (c) 2012 Cerios Game Studios. All rights reserved.
//

#import "ScoreFooterCell.h"

@implementation ScoreFooterCell
@synthesize avgTextField;
@synthesize totalTextField;
@synthesize hitsTextField;

- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if (self) {
        avgTextField = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 80, 22)];
        totalTextField = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 80, 22)];
        hitsTextField = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 80, 22)];
        
        [avgTextField setText:@"Avg: 0.0"];
        [avgTextField setFont:[avgTextField.font fontWithSize:11.0]];
        [avgTextField setTextAlignment:NSTextAlignmentCenter];
        [avgTextField setTranslatesAutoresizingMaskIntoConstraints:NO];

        [totalTextField setBackgroundColor:[UIColor colorWithRed:0 green:.760844466 blue:1 alpha:1]];
        [totalTextField setTextAlignment:NSTextAlignmentCenter];
        [totalTextField setTranslatesAutoresizingMaskIntoConstraints:NO];
        [totalTextField setText:@"0"];

        [hitsTextField setText:@"Hits: 0/0"];
        [hitsTextField setTranslatesAutoresizingMaskIntoConstraints:NO];
        
        [self.contentView addSubview:avgTextField];
        [self.contentView addSubview:totalTextField];
        [self.contentView addSubview:hitsTextField];
        
        NSDictionary *views = NSDictionaryOfVariableBindings(avgTextField, totalTextField, hitsTextField);
        NSMutableArray *constraints = [[NSMutableArray alloc] init];
        [constraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[hitsTextField]" options:NSLayoutFormatAlignAllCenterY metrics:nil views:views]];
        [constraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[totalTextField(>=70,<=100)]-3-|" options:NSLayoutFormatAlignAllCenterY metrics:nil views:views]];
        [constraints addObject:[NSLayoutConstraint constraintWithItem:avgTextField attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0]];
        [constraints addObject:[NSLayoutConstraint constraintWithItem:avgTextField attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0]];
        [constraints addObject:[NSLayoutConstraint constraintWithItem:totalTextField attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0]];
        [constraints addObject:[NSLayoutConstraint constraintWithItem:hitsTextField attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0]];
        [self.contentView addConstraints:constraints];
    }
    return self;
}

@end
