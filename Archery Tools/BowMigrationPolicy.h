//
//  BowMigrationPolicy.h
//  Archery Tools
//
//  Created by Andrew Querol on 5/19/14.
//  Copyright (c) 2014 Cerios Game Studios. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface BowMigrationPolicy : NSEntityMigrationPolicy

@end
