//
//  EndInfoViewController.h
//  Archery Tools
//
//  Created by Andrew Querol on 4/3/12.
//  Copyright (c) 2012 Cerios Game Studios. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface EndInfoViewController : UIViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@property (nonatomic) UIImagePickerController *imgPicker;
@property (weak, nonatomic) IBOutlet UIImageView *targetPictureView;
@property NSString *imageURL;

- (IBAction)cameraButton:(id)sender;
- (IBAction)clearPhotoButton:(id)sender;
- (IBAction)backButton:(id)sender;

@end
