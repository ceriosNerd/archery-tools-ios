//
//  LegalViewController.h
//  Archery Tools
//
//  Created by Andrew Querol on 12/24/14.
//  Copyright (c) 2014 Cerios Game Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LegalViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextView *legalTextView;
@end
