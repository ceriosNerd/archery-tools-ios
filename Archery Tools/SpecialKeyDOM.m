//
//  SpecialKeyDOM.m
//  Archery Tools
//
//  Created by Andrew Querol on 5/19/14.
//  Copyright (c) 2014 Cerios Game Studios. All rights reserved.
//

#import "SpecialKeyDOM.h"

@implementation SpecialKeyDOM
@synthesize code;
@synthesize height;
@synthesize image;
@synthesize width;
@end
