//
//  ArcheryTextField.h
//  Archery Tools
//
//  Created by Andrew Querol on 3/27/12.
//  Copyright (c) 2012 Cerios Game Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ScoreEndCell;

@interface ArcheryTextLabel : UILabel

@property UIView *keyboardView;
@property ScoreEndCell *parent;

@end
