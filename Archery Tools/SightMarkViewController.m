//
//  SightMarksViewController.m
//  Archery Tools
//
//  Created by Andrew Querol on 3/24/12.
//  Copyright (c) 2012 Cerios Game Studios. All rights reserved.
//

#import "SightMarkViewController.h"
#import "GenerateViewController.h"
#import "SQLiteSingleton.h"

@implementation SightMarkViewController

@synthesize sightConfigId;

- (void)viewDidAppear:(BOOL)animated {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (![defaults boolForKey:@"shakeShown"] && ![self.view.layer valueForKey:@"shook"]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Tip!" message:@"Shake to set this sight configuration for quick access. Then shake from anywhere else to get back fast! To change the quick access just shake inside another sight configuration." delegate:self cancelButtonTitle:@"Done" otherButtonTitles:@"Disable this Message", nil];
        [alert.layer setValue:[NSNumber numberWithBool:TRUE] forKey:@"shakeMsg"];
        [alert show];
    }
    [self becomeFirstResponder];
    [super viewDidAppear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    [self resignFirstResponder];
    [super viewDidDisappear:animated];
}

- (void)motionBegan:(UIEventSubtype)motion withEvent:(UIEvent *)event {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:sightConfigId forKey:@"defaultSight"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[[UIAlertView alloc] initWithTitle:@"Quick Access!" message:@"Quick access set to this sight configuration!" delegate:nil cancelButtonTitle:@"Done" otherButtonTitles:nil] show];
}

- (BOOL)canBecomeFirstResponder {
    return YES;
}

- (NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section {
    sqlite3_stmt *sightMarkStatement;
    NSString *sightMarkQuery = [NSString stringWithFormat:@"SELECT COUNT(*) FROM sight_marks WHERE `sight_config_id`='%lli'", [sightConfigId longLongValue]];
    sqlite3_prepare_v2([[SQLiteSingleton getInstance] getDatabase], [sightMarkQuery UTF8String], -1, &sightMarkStatement, nil);
    
    int count = 0;
    if (sqlite3_step(sightMarkStatement) == SQLITE_ROW)
        count = sqlite3_column_int(sightMarkStatement, 0);
    sqlite3_finalize(sightMarkStatement);
    
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"sightMark"];
    
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return TRUE;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        sqlite3_exec([[SQLiteSingleton getInstance] getDatabase], [[NSString stringWithFormat:@"DELETE FROM sight_marks WHERE `id`=(SELECT id FROM sight_marks WHERE `sight_config_id`='%lli' ORDER BY smaller_units DESC, distance ASC LIMIT 1 OFFSET %ld)", [sightConfigId longLongValue], (long)[indexPath row]] UTF8String], nil, nil, nil);
    }
    [self.tableView reloadData];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    sqlite3_stmt *updateSightMarkStatement;
    NSString *updateSightMarkQuery = [NSString stringWithFormat:@"SELECT distance, mark, id FROM sight_marks WHERE `sight_config_id`='%lli' ORDER BY smaller_units DESC, distance ASC LIMIT 1 OFFSET %li", [sightConfigId longLongValue], (long) indexPath.row];
    sqlite3_prepare_v2([[SQLiteSingleton getInstance] getDatabase], [updateSightMarkQuery UTF8String], -1, &updateSightMarkStatement, nil);
    
    if (sqlite3_step(updateSightMarkStatement) != SQLITE_ROW) {
        NSLog(@"Failed to retreive the sight mark at: %li", (long) indexPath.row);
        return;
    }
    
    UIAlertView *editMark = [[UIAlertView alloc] initWithTitle:@"Edit Sight Mark" message:nil delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Done", nil];
    [editMark setAlertViewStyle:UIAlertViewStyleLoginAndPasswordInput];
    [[editMark textFieldAtIndex:0] setText:[[NSNumber numberWithInt:sqlite3_column_int(updateSightMarkStatement, 0)] stringValue]];
    [[editMark textFieldAtIndex:0] setPlaceholder:@"Distance"];
    [[editMark textFieldAtIndex:0] setKeyboardType:UIKeyboardTypeNumberPad];
    [[editMark textFieldAtIndex:1] setText:[[NSNumber numberWithDouble:sqlite3_column_double(updateSightMarkStatement, 1)] stringValue]];
    [[editMark textFieldAtIndex:1] setKeyboardType:UIKeyboardTypeDecimalPad];
    [[editMark textFieldAtIndex:1] setPlaceholder:@"Sight Mark"];
    [[editMark textFieldAtIndex:1] setSecureTextEntry:false];
    [editMark.layer setValue:[NSNumber numberWithLongLong:sqlite3_column_int64(updateSightMarkStatement, 2)] forKey:@"edit"];
    sqlite3_finalize(updateSightMarkStatement);
    [editMark show];
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    sqlite3_stmt *sightMarkStatement;
    NSString *sightMarkQuery = [NSString stringWithFormat:@"SELECT distance, mark, smaller_units FROM sight_marks WHERE `sight_config_id`='%lli' ORDER BY smaller_units DESC, distance ASC LIMIT 1 OFFSET %li", [sightConfigId longLongValue], (long) indexPath.row];
    sqlite3_prepare_v2([[SQLiteSingleton getInstance] getDatabase], [sightMarkQuery UTF8String], -1, &sightMarkStatement, nil);
    
    if (sqlite3_step(sightMarkStatement) != SQLITE_ROW) {
        NSLog(@"Failed to retreive the sight mark at: %li", (long) indexPath.row);
        return;
    }
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setMaximumFractionDigits:2];
    cell.textLabel.text = [[NSNumber numberWithInt:sqlite3_column_int(sightMarkStatement, 0)] stringValue];
    cell.detailTextLabel.text = [formatter stringFromNumber:[NSNumber numberWithDouble:sqlite3_column_double(sightMarkStatement, 1)]];
    if (sqlite3_column_int(sightMarkStatement, 2))
        [cell.textLabel setText:[cell.textLabel.text stringByAppendingString:@" Ft"]];
    sqlite3_finalize(sightMarkStatement);
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.destinationViewController isKindOfClass:[GenerateViewController class]]) {
        GenerateViewController *generate = segue.destinationViewController;
        [generate setSightConfigId:sightConfigId];
    }
    [self setEditing:FALSE];
}

- (IBAction)addClicked:(id)sender {
    UIAlertView *newMark = [[UIAlertView alloc] initWithTitle:@"New Sight Mark" message:@"Set the sight mark here or press generate to generate sight marks." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Add", @"Generate", nil];
    [newMark setAlertViewStyle:UIAlertViewStyleLoginAndPasswordInput];
    [[newMark textFieldAtIndex:0] setPlaceholder:@"Distance"];
    [[newMark textFieldAtIndex:0] setKeyboardType:UIKeyboardTypeNumberPad];
    [[newMark textFieldAtIndex:1] setPlaceholder:@"Sight Mark"];
    [[newMark textFieldAtIndex:1] setKeyboardType:UIKeyboardTypeDecimalPad];
    [[newMark textFieldAtIndex:1] setSecureTextEntry:false];
    [newMark show];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if ([alertView.layer valueForKey:@"shakeMsg"]) {
        if (buttonIndex == 1) {
            [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"shakeShown"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        return;
    }
    if (buttonIndex == 0)
        return;
    if (buttonIndex == 2) {
        [self performSegueWithIdentifier:@"generatePush" sender:nil];
        return;
    }
    
    if ([alertView.layer valueForKey:@"edit"]) {
        long long editId = [(NSNumber *)[alertView.layer valueForKey:@"edit"] longLongValue];
        sqlite3_exec([[SQLiteSingleton getInstance] getDatabase], [[NSString stringWithFormat:@"UPDATE sight_marks SET `distance`='%i', `mark`='%f', `synced`='0' WHERE `id`=%lli", [[[alertView textFieldAtIndex:0] text] intValue], [[NSDecimalNumber decimalNumberWithString:[[alertView textFieldAtIndex:1] text]] floatValue], editId] UTF8String], nil, nil, nil);
    } else {
        sqlite3_exec([[SQLiteSingleton getInstance] getDatabase], [[NSString stringWithFormat:@"INSERT INTO sight_marks(distance, mark, sight_config_id) VALUES ('%i', '%f', '%lli');", [[[alertView textFieldAtIndex:0] text] intValue], [[NSDecimalNumber decimalNumberWithString:[[alertView textFieldAtIndex:1] text]] floatValue], [sightConfigId longLongValue]] UTF8String], nil, nil, nil);
    }
    [self.tableView reloadData];
}

- (BOOL)alertViewShouldEnableFirstOtherButton:(UIAlertView *)alertView {
    if ([alertView alertViewStyle] <= 0)
        return true;
    if([[[alertView textFieldAtIndex:0] text] length] > 0 && [[[alertView textFieldAtIndex:1] text] length] > 0) {
        return YES;
    } else {
        return NO;
    }
}

@end
