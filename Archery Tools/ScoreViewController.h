//
//  ScoreViewController.h
//  Archery Tools
//
//  Created by Andrew Querol on 3/27/12.
//  Copyright (c) 2012 Cerios Game Studios. All rights reserved.
//

#import "ScoreEndCell.h"

@class KeyboardGenerator;

@interface ScoreViewController: UITableViewController<ScoreEndCellDelgate>

@property KeyboardGenerator *generator;
@property UIView *keyboard;

- (void) setScoreCardId:(int) scoreCardId;

@end
