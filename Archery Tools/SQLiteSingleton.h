//
//  SQLiteSingleton.h
//  Archery Tools
//
//  Created by Andrew Querol on 11/23/14.
//  Copyright (c) 2014 Cerios Game Studios. All rights reserved.
//

#import <Foundation/Foundation.h>
#include <sqlite3.h>

typedef enum : NSUInteger {
    CoreDataError,
    SQLiteError,
} FailCode;

typedef enum : NSUInteger {
    NoCoreData,
    MigrationDone,
} SuccessCode;

typedef void(^onSuccess)(SuccessCode code);
typedef void(^onFailure)(FailCode code);

@interface SQLiteSingleton : NSObject

@property (nonatomic, strong) NSMutableDictionary *dirtyTables;

+ (SQLiteSingleton *) getInstance;
- (sqlite3 *) getDatabase;
- (void) tryToMigrateFromCoreData:(NSPersistentStoreCoordinator *)persistantStore onSuccess:(onSuccess)sucessBlock onFailure:(onFailure)failBlock;
+ (NSString *) parseString:(NSString *) input;
@end
