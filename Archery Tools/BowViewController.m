//
//  BowViewController.m
//  Archery Tools
//
//  Created by Andrew Querol on 5/17/14.
//  Copyright (c) 2014 Cerios Game Studios. All rights reserved.
//

#import "BowViewController.h"
#import "SQLiteSingleton.h"
#import "BowPageViewController.h"

@implementation BowViewController

- (NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section {
    sqlite3_stmt *bowsStatement;
    NSString *bowsQuery = @"SELECT COUNT(*) FROM bows";
    sqlite3_prepare_v2([[SQLiteSingleton getInstance] getDatabase], [bowsQuery UTF8String], -1, &bowsStatement, nil);
    
    int count = 0;
    if (sqlite3_step(bowsStatement) == SQLITE_ROW)
        count = sqlite3_column_int(bowsStatement, 0);
    sqlite3_finalize(bowsStatement);

    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"bow"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"bow"];
    }
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return TRUE;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView reloadData];
}

- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewRowAction *moreAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Edit" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        UIAlertView *newBowAlert = [[UIAlertView alloc] initWithTitle:@"Edit Bow Name" message:nil delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Done", nil];
        [newBowAlert setAlertViewStyle:UIAlertViewStylePlainTextInput];
        [[newBowAlert textFieldAtIndex:0] setText:[self tableView:tableView cellForRowAtIndexPath:indexPath].textLabel.text];
        [newBowAlert.layer setValue:[NSNumber numberWithUnsignedInteger:indexPath.row] forKey:@"edit"];
        [newBowAlert show];
        [self.tableView setEditing:NO];
    }];
    moreAction.backgroundColor = [UIColor lightGrayColor];
    
    UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive title:@"Delete"  handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
        sqlite3_exec([[SQLiteSingleton getInstance] getDatabase], [[NSString stringWithFormat:@"DELETE FROM bows WHERE `id`=(SELECT id FROM bows ORDER BY id ASC LIMIT 1 OFFSET %ld)", (long)[indexPath row]] UTF8String], nil, nil, nil);
        [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    }];
    
    return @[deleteAction, moreAction];
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    sqlite3_stmt *bowStatement;
    NSString *bowQuery = [NSString stringWithFormat:@"SELECT name FROM bows ORDER BY id ASC LIMIT 1 OFFSET %li", (long) indexPath.row];
    sqlite3_prepare_v2([[SQLiteSingleton getInstance] getDatabase], [bowQuery UTF8String], -1, &bowStatement, nil);
    
    if (sqlite3_step(bowStatement) != SQLITE_ROW) {
        NSLog(@"Failed to retreive the bow name for bow ID: %li", (long) indexPath.row);
        return;
    }
    
    cell.textLabel.text = [NSString stringWithUTF8String:(const char*)sqlite3_column_text(bowStatement, 0)];
    sqlite3_finalize(bowStatement);
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.destinationViewController class] == [BowPageViewController class]) {
        BowPageViewController *controller = segue.destinationViewController;
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        sqlite3_stmt *bowIdStatement;
        NSString *bowIdQuery = [NSString stringWithFormat:@"SELECT id FROM bows ORDER BY id ASC LIMIT 1 OFFSET %ld", (long)indexPath.row];
        sqlite3_prepare_v2([[SQLiteSingleton getInstance] getDatabase], [bowIdQuery UTF8String], -1, &bowIdStatement, nil);
        if (sqlite3_step(bowIdStatement) != SQLITE_ROW) {
            NSLog(@"Failed to retreive the bow ID!");
            return;
        }
        long long bowId = sqlite3_column_int64(bowIdStatement, 0);
        sqlite3_finalize(bowIdStatement);
        
        [controller setBowId:[NSNumber numberWithLongLong:bowId]];
    }
    [self setEditing:false];
}

- (IBAction)onClick:(id)sender {
    UIAlertView *newBowAlert = [[UIAlertView alloc] initWithTitle:@"New Bow" message:@"Please enter a name for the bow." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Add", nil];
    [newBowAlert setAlertViewStyle:UIAlertViewStylePlainTextInput];
    [[newBowAlert textFieldAtIndex:0] setPlaceholder:@"Bow Name"];
    [newBowAlert show];
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {
    UIAlertView *newBowAlert = [[UIAlertView alloc] initWithTitle:@"Edit Bow Name" message:nil delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Done", nil];
    [newBowAlert setAlertViewStyle:UIAlertViewStylePlainTextInput];
    [[newBowAlert textFieldAtIndex:0] setText:[self tableView:tableView cellForRowAtIndexPath:indexPath].textLabel.text];
    [newBowAlert.layer setValue:[NSNumber numberWithUnsignedInteger:indexPath.row] forKey:@"edit"];
    [newBowAlert show];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0)
        return;
    NSString *bowName = [[alertView textFieldAtIndex:0] text];
    
    if ([alertView.layer valueForKey:@"edit"]) {
        long long editId = [(NSNumber *)[alertView.layer valueForKey:@"edit"] longLongValue];
        sqlite3_exec([[SQLiteSingleton getInstance] getDatabase], [[NSString stringWithFormat:@"UPDATE bows SET `name`='%@', `synced`='0' WHERE `id`=(SELECT id FROM bows ORDER BY id ASC LIMIT 1 OFFSET %lli)", bowName, editId] UTF8String], nil, nil, nil);
    } else {
        sqlite3_exec([[SQLiteSingleton getInstance] getDatabase], [[NSString stringWithFormat:@"INSERT INTO bows(name) VALUES ('%@');", bowName] UTF8String], nil, nil, nil);
    }
    
    [self.tableView reloadData];
}

- (BOOL)alertViewShouldEnableFirstOtherButton:(UIAlertView *)alertView {
    NSString *inputText = [[alertView textFieldAtIndex:0] text];
    if([inputText length] > 0) {
        return YES;
    } else {
        return NO;
    }
}

@end
