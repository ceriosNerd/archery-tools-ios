//
//  KeyDOM.h
//  Archery Tools
//
//  Created by Andrew Querol on 5/16/14.
//  Copyright (c) 2014 Cerios Game Studios. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KeyDOM : NSObject

@property NSString *keyText;
@property NSNumber *keyValue;
@property NSNumber *keyRank; // 1 = highest, -1 = miss
@property NSString *imageFile;
@property NSString *textColor;

@end
