//
//  RoundDOM.m
//  Archery Tools
//
//  Created by Andrew Querol on 5/15/14.
//  Copyright (c) 2014 Cerios Game Studios. All rights reserved.
//

#import "RoundDOM.h"

@implementation RoundDOM

@synthesize arrowsPerEnd;
@synthesize endsPerRound;
@synthesize distance;

@end
