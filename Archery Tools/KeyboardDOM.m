//
//  KeyboardDOM.m
//  Archery Tools
//
//  Created by Andrew Querol on 5/16/14.
//  Copyright (c) 2014 Cerios Game Studios. All rights reserved.
//

#import "KeyboardDOM.h"

@implementation KeyboardDOM
@synthesize name;
@synthesize rows;
@synthesize fileName;
@synthesize specialKeys;
@end
