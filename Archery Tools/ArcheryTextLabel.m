//
//  ArcheryTextField.m
//  Archery Tools
//
//  Created by Andrew Querol on 3/27/12.
//  Copyright (c) 2012 Cerios Game Studios. All rights reserved.
//

#import "ArcheryTextLabel.h"
#import "ScoreEndCell.h"

@implementation ArcheryTextLabel

@synthesize keyboardView;
@synthesize parent;

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        [self setTextAlignment:NSTextAlignmentCenter];
        [self setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"gray"]]];
        [self setUserInteractionEnabled:YES];
    }
    return self;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self becomeFirstResponder];
    [super touchesBegan:touches withEvent:event];
}

- (UIView *)inputView {
    return keyboardView;
}

- (BOOL)resignFirstResponder {
    [parent clearSelection];
    [super resignFirstResponder];
    return true;
}

-(BOOL)canBecomeFirstResponder {
    return YES;
}

@end
