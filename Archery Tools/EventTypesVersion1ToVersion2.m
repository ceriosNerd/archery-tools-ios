//
//  EventTypesVersion1ToVersion2.m
//  Archery Tools
//
//  Created by Andrew Querol on 4/19/12.
//  Copyright (c) 2012 Cerios Game Studios. All rights reserved.
//

#import "EventTypesVersion1ToVersion2.h"

@implementation EventTypesVersion1ToVersion2

- (BOOL)createRelationshipsForDestinationInstance:(NSManagedObject *)dInstance entityMapping:(NSEntityMapping *)mapping manager:(NSMigrationManager *)manager error:(NSError **)error {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"EventType" inManagedObjectContext:[manager sourceContext]];
    [fetchRequest setEntity:entity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"parent.group = %@", [dInstance valueForKey:@"group"]];
    [fetchRequest setPredicate:predicate];
    
    NSArray *oldEventTypes = [[manager sourceContext] executeFetchRequest:fetchRequest error:nil];
    
    for (NSManagedObject *oldEventType in oldEventTypes) {
        
        if ([oldEventType valueForKey:@"parent"] == nil) {
            continue;
        }
        
        NSManagedObject *newEventType = [NSEntityDescription insertNewObjectForEntityForName:@"EventType" inManagedObjectContext:[manager destinationContext]];

        for (int i = 0; i < [[oldEventType valueForKey:@"rounds"] integerValue]; i++) {
            NSManagedObject *round = [NSEntityDescription insertNewObjectForEntityForName:@"EventTypeRound"inManagedObjectContext:[manager destinationContext]];
            [round setValue:[oldEventType valueForKey:@"arrowsPerEnd"] forKey:@"arrowsPerEnd"];
            [round setValue:[oldEventType valueForKey:@"endsPerRound"] forKey:@"endsPerRound"];
            [round setValue:[oldEventType valueForKey:@"keyboardType"] forKey:@"keyboardType"];
            [round setValue:newEventType forKey:@"parent"];
            [[newEventType mutableSetValueForKey:@"rounds"] addObject:round];
        }
        [newEventType setValue:[oldEventType valueForKey:@"name"] forKey:@"name"];
        [newEventType setValue:dInstance forKey:@"parent"];
        [[dInstance mutableSetValueForKey:@"eventTypes"] addObject:newEventType];
    }
    return [super createRelationshipsForDestinationInstance:dInstance entityMapping:mapping manager:manager error:error];
}

@end
