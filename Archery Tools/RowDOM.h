//
//  RowDOM.h
//  Archery Tools
//
//  Created by Andrew Querol on 5/16/14.
//  Copyright (c) 2014 Cerios Game Studios. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RowDOM : NSObject

@property NSMutableOrderedSet *keys;

@end
