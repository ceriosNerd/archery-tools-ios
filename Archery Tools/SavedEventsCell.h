//
//  SavedEventsCell.h
//  Archery Tools
//
//  Created by Andrew Querol on 1/6/15.
//  Copyright (c) 2015 Cerios Game Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SavedEventsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleTextField;
@property (weak, nonatomic) IBOutlet UILabel *dateTextField;
@end
