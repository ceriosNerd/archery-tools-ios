//
//  ScoreHeaderCell.m
//  Archery Tools
//
//  Created by Andrew Querol on 3/28/12.
//  Copyright (c) 2012 Cerios Game Studios. All rights reserved.
//

#import "ScoreHeaderCell.h"

@implementation ScoreHeaderCell

- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if (self) {
        UILabel *runningTotal = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 22, 22)];
        [runningTotal setText:@"R/T"];
        UILabel *arrows = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 80, 22)];
        [arrows setText:@"Arrows"];
        
        [runningTotal setTranslatesAutoresizingMaskIntoConstraints:NO];
        [arrows setTranslatesAutoresizingMaskIntoConstraints:NO];
        
        [self.contentView addSubview:runningTotal];
        [self.contentView addSubview:arrows];
        
        NSDictionary *views = @{@"runningTotal": runningTotal};
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[runningTotal]-|" options:NSLayoutFormatAlignAllCenterY metrics:nil views:views]];
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:runningTotal attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0]];
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:arrows attribute:NSLayoutAttributeCenterXWithinMargins relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeCenterXWithinMargins multiplier:1.0 constant:0]];
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:arrows attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0]];
    }
    return self;
}

@end
