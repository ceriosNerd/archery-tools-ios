//
//  SightConfigTableViewCell.m
//  Archery Tools
//
//  Created by Andrew Querol on 5/18/14.
//  Copyright (c) 2014 Cerios Game Studios. All rights reserved.
//

#import "SightConfigTableViewCell.h"
#import "SQLiteSingleton.h"

@implementation SightConfigTableViewCell

@synthesize sightConfigId;
@synthesize unitsSegmentedControl;

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (IBAction)onUnitsChanged:(UISegmentedControl *)sender {
    sqlite3_exec([[SQLiteSingleton getInstance] getDatabase], [[NSString stringWithFormat:@"UPDATE sight_configurations SET `metric`='%i', `synced`='0' WHERE `id`=%lli", [sender selectedSegmentIndex] == 0, [sightConfigId longLongValue]] UTF8String], nil, nil, nil);
}
@end
