//
//  KeyboardGenerator.h
//  Archery Tools
//
//  Created by Andrew Querol on 5/14/14.
//  Copyright (c) 2014 Cerios Game Studios. All rights reserved.
//

#import <Foundation/Foundation.h>

@class KeyboardDOM, RowDOM;

@interface KeyboardGenerator : NSObject <NSXMLParserDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property KeyboardDOM *tempKeyboard;
@property RowDOM *currentRow;

- (KeyboardDOM *) generateKeyboardFromFile:(NSString *)keyboardFile;
- (UIView *) generateKeyboardViewFromDOM:(KeyboardDOM *)keyboardDOM;

@end
