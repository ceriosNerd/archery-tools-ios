//
//  SightMarksViewController.m
//  Archery Tools
//
//  Created by Andrew Querol on 3/24/12.
//  Copyright (c) 2012 Cerios Game Studios. All rights reserved.
//

#import "SightMarksViewController.h"
#import "SightMarkViewController.h"
#import "SightConfigTableViewCell.h"
#import "SQLiteSingleton.h"

@implementation SightMarksViewController

@synthesize bowId;

- (IBAction)onAddClick:(UIBarButtonItem *)sender {
    UIAlertView *newSight = [[UIAlertView alloc] initWithTitle:@"New Sight Configuration" message:@"Please enter a name for the sight configuration" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Add", nil];
    [newSight setAlertViewStyle:UIAlertViewStylePlainTextInput];
    [[newSight textFieldAtIndex:0] setPlaceholder:@"Configuration Name"];
    [newSight show];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0)
        return;
    NSString *configName = [[alertView textFieldAtIndex:0] text];
    if ([alertView.layer valueForKey:@"edit"] != nil) {
        NSNumber *configId = [alertView.layer valueForKey:@"edit"];
        sqlite3_exec([[SQLiteSingleton getInstance] getDatabase], [[NSString stringWithFormat:@"UPDATE sight_configurations SET `name`='%@', `synced`='0' WHERE `id`=(SELECT id FROM sight_configurations WHERE `bow_id`='%lli' ORDER BY id ASC LIMIT 1 OFFSET %lli)", configName, [bowId longLongValue], [configId longLongValue]] UTF8String], nil, nil, nil);
    } else {
        sqlite3_exec([[SQLiteSingleton getInstance] getDatabase], [[NSString stringWithFormat:@"INSERT INTO sight_configurations(name, metric, bow_id) VALUES ('%@', '%i', '%lli');", configName, true, [bowId longLongValue]] UTF8String], nil, nil, nil);
    }
    
    [self.tableView reloadData];
}

- (BOOL)alertViewShouldEnableFirstOtherButton:(UIAlertView *)alertView {
    NSString *inputText = [[alertView textFieldAtIndex:0] text];
    if([inputText length] > 0) {
        return YES;
    } else {
        return NO;
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return true;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView reloadData];
}

- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewRowAction *moreAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Edit" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        UIAlertView *sightConfigEdit = [[UIAlertView alloc] initWithTitle:@"Edit Name" message:nil delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Done", nil];
        [sightConfigEdit.layer setValue:[NSNumber numberWithUnsignedInteger:indexPath.row] forKey:@"edit"];
        [sightConfigEdit setAlertViewStyle:UIAlertViewStylePlainTextInput];
        [[sightConfigEdit textFieldAtIndex:0] setText:[self tableView:tableView cellForRowAtIndexPath:indexPath].textLabel.text];
        [sightConfigEdit show];
    }];
    moreAction.backgroundColor = [UIColor lightGrayColor];
    
    UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive title:@"Delete"  handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
        sqlite3_exec([[SQLiteSingleton getInstance] getDatabase], [[NSString stringWithFormat:@"DELETE FROM sight_configurations WHERE `id`=(SELECT id FROM sight_configurations WHERE `bow_id`='%lli' ORDER BY id ASC LIMIT 1 OFFSET %ld)", [bowId longLongValue], (long)[indexPath row]] UTF8String], nil, nil, nil);
        [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    }];
    
    return @[deleteAction, moreAction];
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {
    UIAlertView *sightConfigEdit = [[UIAlertView alloc] initWithTitle:@"Edit Name" message:nil delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Done", nil];
    [sightConfigEdit.layer setValue:[NSNumber numberWithUnsignedInteger:indexPath.row] forKey:@"edit"];
    [sightConfigEdit setAlertViewStyle:UIAlertViewStylePlainTextInput];
    [[sightConfigEdit textFieldAtIndex:0] setText:[self tableView:tableView cellForRowAtIndexPath:indexPath].textLabel.text];
    [sightConfigEdit show];
}

- (NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section {
    sqlite3_stmt *sightConfigsStatement;
    NSString *sightConfigsQuery = [NSString stringWithFormat:@"SELECT COUNT(*) FROM sight_configurations WHERE `bow_id`='%lli'", [bowId longLongValue]];
    sqlite3_prepare_v2([[SQLiteSingleton getInstance] getDatabase], [sightConfigsQuery UTF8String], -1, &sightConfigsStatement, nil);
    
    int count = 0;
    if (sqlite3_step(sightConfigsStatement) == SQLITE_ROW)
        count = sqlite3_column_int(sightConfigsStatement, 0);
    sqlite3_finalize(sightConfigsStatement);
    
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SightConfigTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"sightMarks"];
    if (cell == nil) {
        cell = [[SightConfigTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"sightMarks"];
    }
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (void) configureCell:(SightConfigTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    sqlite3_stmt *sightConfigsStatement;
    NSString *sightConfigsQuery = [NSString stringWithFormat:@"SELECT id, name, metric FROM sight_configurations WHERE `bow_id`='%lli' ORDER BY id ASC LIMIT 1 OFFSET %li", [bowId longLongValue], (long) indexPath.row];
    sqlite3_prepare_v2([[SQLiteSingleton getInstance] getDatabase], [sightConfigsQuery UTF8String], -1, &sightConfigsStatement, nil);
    
    if (sqlite3_step(sightConfigsStatement) != SQLITE_ROW) {
        NSLog(@"Failed to retreive the sight configuration at: %li", (long) indexPath.row);
        return;
    }
    
    cell.textLabel.text = [NSString stringWithUTF8String:(const char *)sqlite3_column_text(sightConfigsStatement, 1)];
    
    [cell.unitsSegmentedControl setSelectedSegmentIndex:!sqlite3_column_int(sightConfigsStatement, 2)];
    [cell setSightConfigId:[NSNumber numberWithLongLong:sqlite3_column_int64(sightConfigsStatement, 0)]];
    sqlite3_finalize(sightConfigsStatement);
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.destinationViewController class] == [SightMarkViewController class]) {
        SightMarkViewController *controller = (SightMarkViewController *) segue.destinationViewController;
        sqlite3_stmt *sightConfigIdStatement;
        NSString *sightConfigIdQuery = [NSString stringWithFormat:@"SELECT id FROM sight_configurations WHERE `bow_id`='%lli' ORDER BY id ASC LIMIT 1 OFFSET %ld", [bowId longLongValue], (long)[self.tableView indexPathForSelectedRow].row];
        sqlite3_prepare_v2([[SQLiteSingleton getInstance] getDatabase], [sightConfigIdQuery UTF8String], -1, &sightConfigIdStatement, nil);
        if (sqlite3_step(sightConfigIdStatement) != SQLITE_ROW) {
            NSLog(@"Failed to retreive the sight configuration ID!");
            return;
        }
        long long sightConfigId = sqlite3_column_int64(sightConfigIdStatement, 0);
        sqlite3_finalize(sightConfigIdStatement);
        [controller setSightConfigId: [NSNumber numberWithLongLong:sightConfigId]];
    }
    [self setEditing:FALSE];
}

@end
